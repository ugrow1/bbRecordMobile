export { default as MenuHeader } from "./MenuHeader";
export { default as ViewsHeader } from "./ViewsHeader";
export { default as SearchBar } from "./SearchBar";
export {
  ImageInput,
  SelectInput,
  DateInput,
  DefaultInput,
} from "./RegisterFields";
export { default as BabiesTabNavigator } from "./BabySection/BabiesTabNavigator";
export { default as BabySection } from "./BabySection/BabySection";
export { default as BabySectionActions } from "./BabySection/BabySectionActions";
export { default as Notifications } from "./BabySection/Notifications";

export { takePhoto, cameraPermissions } from "./Camera";
export {
  SplitscreenIcon,
  MedicationLiquidIcon,
  HealthAndSafetyIcon,
  VaccinesIcon,
  AccountCircleOutline,
} from "./CustomIcons";
export { default as Appointment } from "./RecordViews/Appointment/Appointment";
export { default as SufferedDiseases } from "./RecordViews/Disease/SufferedDiseases";
export { default as Report } from "./RecordViews/Report/Reports";
export { default as Injury } from "./RecordViews/Injury/Injuries";
export { default as Vaccine } from "./RecordViews/Vaccine/Vaccines";
export { default as Prescription } from "./RecordViews/Prescription/Prescriptions";
export { default as RecordDetails } from "./RecordViews/RecordDetails";
export { default as AppointmentDetails } from "./RecordViews/Appointment/AppointmentDetails";
export { default as PrescriptionDetails } from "./RecordViews/Prescription/PrescriptionDetails";
export { default as VaccineDetails } from "./RecordViews/Vaccine/VaccineDetails";
export { default as ReportDetails } from "./RecordViews/Report/ReportDetails";
