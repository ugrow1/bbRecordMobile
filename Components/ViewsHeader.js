import React, { useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import {
  HStack,
  IconButton,
  Icon,
  Text,
  Box,
  StatusBar,
  Pressable,
  Image,
  Center,
} from "native-base";
import { AuthUserContext, route, socket, useScanDoctor } from "../Hooks";
import { MaterialIcons } from "@expo/vector-icons";
import { StyleSheet } from "react-native";
import axios from "axios";
import { style } from "styled-system";

export default function ViewsHeader({
  viewTitle,
  canGoBack,
  canLogOut,
  noImage,
  color,
  baby,
}) {
  const navigation = useNavigation();
  const { userData, removeItem, setLogged } = useContext(AuthUserContext);
  const logout = async () => {
    setLogged(false);
  };
  const displayedImage = baby
    ? baby.image
      ? { uri: route + baby.image.url }
      : require("../assets/defaultUserImage.png")
    : userData.user.meta_user.image
    ? { uri: route + userData.user.meta_user.image.url }
    : require("../assets/defaultUserImage.png");
  return (
    <>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />

      <Box safeAreaTop backgroundColor="#ffffff" />
      <HStack
        bg="#ffffff"
        borderColor="#f0f0f0"
        borderBottomWidth={1}
        px={1}
        flex={1}
        alignItems="flex-end"
        width="100%"
      >
        {/* ICON TOP LEFT */}
        <Box flex={1} alignItems="center" justifyContent="center" h="100%">
          {!noImage && (
            <>
              <Image
                source={displayedImage}
                alt="Alternate Text"
                rounded="full"
                size="37px"
                mb={1}
              />
              <Text fontSize="xs" color="#ACACAC">
                {baby ? baby.name.split(" ")[0] : "Yo"}
              </Text>
            </>
          )}
        </Box>

        {/* VIEW TITTLE */}
        <Box
          my={2}
          flex={3}
          h="100%"
          alignItems="center"
          justifyContent="center"
          bg="#fff"
          borderColor="#f0f0f0"
          borderLeftWidth={1}
          borderRightWidth={1}
        >
          <Text fontSize={23} fontWeight="bold" color={color}>
            {viewTitle}
          </Text>
        </Box>

        {/* VIEW ACTIONS */}

        <HStack
          flex={1}
          h="100%"
          justifyContent="space-evenly"
          alignItems="center"
        >
          {canLogOut && (
            <Pressable variant="viewHeaderButton" flex={1} onPress={logout}>
              <Icon
                as={<MaterialIcons name="login" />}
                size="28px"
                color="#414B54"
              />
            </Pressable>
          )}

          {canGoBack && (
            <Pressable
              flex={1}
              variant="viewHeaderButton"
              onPress={navigation.goBack}
            >
              <Icon
                color="#414B54"
                as={<MaterialIcons name="close" />}
                size="28px"
              />
            </Pressable>
          )}
        </HStack>
      </HStack>
    </>
  );
}
