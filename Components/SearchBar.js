import React, { useContext, useState } from "react";
import { VStack, Input, Icon } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import axios from "axios";
import { AuthUserContext, route } from "../Hooks";

export default function SearchBar({
  height,
  placeHolder = "Buscar",
  endpointToConsult,
  returnResults,
  defaultEntries,
}) {
  const {
    userData: { jwt },
  } = useContext(AuthUserContext);

  const [textValue, setTextValue] = useState();
  //WHEN TEXT IS ENTERED, THEN THIS SEARCHS THE ESPECIFIED ENDPOINT ON IT'S PROPS AND EXECUTES THE SPECIFIED FUNCTION
  const searchEntries = async () => {
    if (textValue == "") {
      returnResults(defaultEntries);
      return;
    }

    //REGEXSEARCH IS FOR REPLACING ALL MATCHES OF THE STRING USED WHEN QUERIES ARE MORE COMPLEX
    //  EX: DOCTORS CAN BE FOUND BY FIRST_NAME OR BY LAST_NAME, SO THE ENDPOINT WILL HAVE "first_name_contains=searchText_or[last_name]=searchText"
    //  THEN WE REPLACE "searchText" WITH THE VALUE GIVEN BY THE USER
    const regexSearch = /searchText/g;
    const endpointToConsultTMP = endpointToConsult.replace(
      regexSearch,
      textValue
    );

    const response = await axios.get(route + endpointToConsultTMP, {
      headers: { Authorization: `Bearer ${jwt}` },
    });
    returnResults(response.data);
  };

  return (
    <VStack width="100%" space={2}>
      <Input
        placeholder={placeHolder}
        variant="filled"
        w="100%"
        onChangeText={(text) => setTextValue(text)}
        onEndEditing={searchEntries}
        bg="#F1F1F3"
        h={height}
        borderRadius={12}
        py={1}
        px={2}
        _web={{
          _focus: { borderColor: "muted.300", style: { boxShadow: "none" } },
        }}
        InputLeftElement={
          <Icon
            size="md"
            ml={2}
            size={5}
            color="gray.400"
            as={<MaterialIcons name="search" />}
          />
        }
        InputRightElement={
          <Icon
            mr={1}
            size={6}
            color="gray.400"
            as={<MaterialIcons name="mic" />}
          />
        }
      />
    </VStack>
  );
}
