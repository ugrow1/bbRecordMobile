import React, { useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import {
  HStack,
  IconButton,
  Icon,
  Text,
  Box,
  StatusBar,
  Pressable,
  Image,
} from "native-base";
import { AuthUserContext, route, socket, useScanDoctor } from "../Hooks";
import { MaterialIcons } from "@expo/vector-icons";
import { StyleSheet } from "react-native";
import axios from "axios";
import { style } from "styled-system";

export default function MenuHeader({
  viewTitle,
  navigateTo,
  canGoBack,
  navigateIcon,
  canLogOut,
  canAccessProfile = false,
  canScanDoctor,
  canAddBabies,
}) {
  const navigation = useNavigation();
  const { userData, removeItem, setLogged } = useContext(AuthUserContext);
  const { onScannedDoctor } = useScanDoctor(
    canScanDoctor && canScanDoctor.babyId
  );
  const logout = async () => {
    setLogged(false);
  };
  return (
    <>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />

      <Box safeAreaTop backgroundColor="#ffffff" />
      <HStack bg="#ffffff" px={1} h="55px" alignItems="center" width="100%">
        <HStack flex={3} space={4} h="100%" alignItems="center">
          {/* Icon on the header */}
          <Pressable
            onPress={() => {
              navigation.navigate("RegisterUser", {
                viewTitle: "Mi perfil",
                modify: true,
              });
            }}
          >
            <Text fontSize={23} fontWeight="bold" ml={5} color="#414B54">
              {viewTitle
                ? viewTitle
                : `${userData.user.first_name} ${userData.user.last_name}`}
            </Text>
          </Pressable>
        </HStack>

        <HStack flex={1} h="100%" alignItems="center">
          {canScanDoctor && (
            <Pressable
              variant="menuHeaderButton"
              onPress={() => {
                navigation.navigate("Scan", {
                  onScan: onScannedDoctor,
                });
              }}
            >
              <Icon
                color="#414B54"
                as={<MaterialIcons name="qr-code-2" />}
                size="28px"
              />
            </Pressable>
          )}

          {canAddBabies && (
            <Pressable
              variant="menuHeaderButton"
              onPress={() => {
                navigation.navigate("RegisterBaby", {
                  modify: false,
                });
              }}
            >
              <Icon
                color="#414B54"
                as={<MaterialIcons name="add-circle-outline" />}
                size="28px"
              />
            </Pressable>
          )}
          {canAccessProfile && (
            <Pressable
              mr={3}
              onPress={() => {
                navigation.navigate("RegisterUser", {
                  viewTitle: "My perfil",
                  modify: true,
                });
              }}
            >
              <Image
                source={{
                  uri: route + userData.user.meta_user.image.url,
                }}
                alt="Imagen"
                size="48px"
              />
            </Pressable>
          )}
          {canLogOut && (
            <HStack space={2}>
              <IconButton
                onPress={logout}
                icon={
                  <Icon
                    as={<MaterialIcons name="login" />}
                    size="md"
                    color="#414B54"
                  />
                }
                size="lg"
              />
            </HStack>
          )}
        </HStack>
      </HStack>
    </>
  );
}
