import React, { useState } from "react";
import {
  FormControl,
  Input,
  HStack,
  Text,
  Stack,
  IconButton,
  Select,
  Icon,
  View,
  Pressable,
  Image,
  Flex,
  Box,
} from "native-base";
import { TouchableOpacity, Modal, Platform } from "react-native";
import { takePhoto } from "./Camera";

import DateTimePicker from "@react-native-community/datetimepicker";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";

export function ImageInput({ source, state, setState }) {
  const handleCamera = async () => {
    const photo = await takePhoto();
    if (state) {
      setState((prevState) => ({ ...prevState, [state]: photo }));
    } else {
      setState(photo);
    }
  };
  return (
    <TouchableOpacity onPress={() => handleCamera()}>
      <HStack>
        {source != undefined ? (
          <Image source={source} rounded="full" alt="Imagen" size={"xl"} />
        ) : (
          <Icon as={<MaterialIcons name="image" />} />
        )}
      </HStack>
    </TouchableOpacity>
  );
}

export function SelectInput({
  label,
  placeholder,
  setState,
  state,
  items,
  value,
  isDisabled,
  isInvalid,
}) {
  const options = items.map((item) => {
    return typeof item == "object" ? (
      <Select.Item
        key={item.id}
        label={item.name ? item.name : `${item.first_name} ${item.last_name}`}
        value={item.id}
      />
    ) : (
      <Select.Item key={item} label={item} value={item} />
    );
  });

  return (
    <Select
      isInvalid={isInvalid}
      isDisabled={isDisabled}
      selectedValue={value}
      variant="formInput"
      placeholder={placeholder}
      onValueChange={(value) => {
        if (state) {
          setState((prevState) => ({ ...prevState, [state]: value }));
        } else {
          setState(value);
        }
      }}
    >
      {options}
    </Select>
  );
}

export function DateInput({ date, setState, state, isDisabled }) {
  const [changeDate, setChangeDate] = useState(false);
  const DatePicker = () => {
    return (
      <DateTimePicker
        is24Hour={true}
        display={Platform.OS === "ios" ? "inline" : "default"}
        value={date}
        onChange={(event, date) => {
          setChangeDate(false);

          if (event.type != "dismissed") {
            setState((prevState) => ({ ...prevState, [state]: date }));
          }
        }}
      />
    );
  };
  return (
    <HStack>
      {Platform.OS == "ios" ? (
        <Modal
          animationType="fade"
          transparent
          visible={changeDate}
          presentationStyle="overFullScreen"
        >
          <Flex bg="rgba(0,0,0,.2)" flex={1} justify="center">
            <View bg="white">
              <DatePicker />
            </View>
          </Flex>
        </Modal>
      ) : (
        changeDate && <DatePicker />
      )}

      <Pressable
        onPress={isDisabled ? undefined : () => setChangeDate(true)}
        flex={4}
      >
        <Box
          isDisabled
          variant="formInput"
          editable={false}
        ><Text>{date.toString().substr(4, 12)}</Text></Box>
      </Pressable>
    </HStack>
  );
}

export function DefaultInput({
  type = "default",
  required = true,
  label,
  setState,
  state,
  hidden,
  value,
}) {
  return (
    <FormControl isRequired={required ? true : undefined}>
      <Stack>
        <Input
          variant="formInput"
          placeholder={label}
          value={value}
          type={hidden ? "password" : ""}
          keyboardType={type}
          onChangeText={(text) => {
            if (state) {
              setState((prevState) => ({
                ...prevState,
                [state]: text,
              }));
            } else {
              setState(text);
            }
          }}
        />

        <FormControl.ErrorMessage>
          I'll only appear when FormControl have isInvalid props.
        </FormControl.ErrorMessage>
      </Stack>
    </FormControl>
  );
}

