import * as ImagePicker from "expo-image-picker";
import { Platform } from "react-native";

export const cameraPermissions = async () => {
  const { status } = await ImagePicker.getCameraPermissionsAsync();
  if (status == "granted") {
    return { cameraAccess: "granted" };
  }

  const librabyPermission =
    await ImagePicker.requestMediaLibraryPermissionsAsync();
  const cameraPermission = await ImagePicker.requestCameraPermissionsAsync();
  if (
    librabyPermission.status !== "granted" ||
    cameraPermission.status !== "granted"
  ) {
    alert("Por favor habilite los permisos de la cámara");
    return;
  } else {
    return { cameraAccess: "granted", libraryAccess: "granted" };
  }
};
export const takePhoto = async () => {
  if (Platform.OS !== "web") {
    const { cameraAccess } = await cameraPermissions();

    if (cameraAccess == "granted" || status == "granted") {
      let photo = ImagePicker.launchCameraAsync({
        allowsEditing: true,
        quality: 1,
      });
      return photo;
    }
  }
};
