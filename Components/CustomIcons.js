import { Rect, Path, G } from "react-native-svg";
import { Icon } from "native-base";
import React from "react";
export const SplitscreenIcon = ({ size, color }) => {
  return (
    <Icon viewBox="0 0 24 24" fill="#000000" size={size} color={color}>
      <G>
        <Path d="M0,0h24v24H0V0z" fill="none" />
      </G>
      <G>
        <G>
          <Path d="M18,4v5H6V4H18 M18,2H6C4.9,2,4,2.9,4,4v5c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V4C20,2.9,19.1,2,18,2z M18,15v5H6v-5H18 M18,13H6c-1.1,0-2,0.9-2,2v5c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2v-5C20,13.9,19.1,13,18,13z" />
        </G>
      </G>
    </Icon>
  );
};
export const VaccinesIcon = ({ size, color, variant }) => {
  return (
    <Icon
      variant={variant}
      viewBox="0 0 24 24"
      fill="#000000"
      size={size}
      color={color}
    >
      <Rect fill="none" height="24" width="24" />
      <Path d="M11,5.5H8V4h0.5c0.55,0,1-0.45,1-1c0-0.55-0.45-1-1-1h-3c-0.55,0-1,0.45-1,1c0,0.55,0.45,1,1,1H6v1.5H3c-0.55,0-1,0.45-1,1 c0,0.55,0.45,1,1,1V15c0,1.1,0.9,2,2,2h1v4l2,1.5V17h1c1.1,0,2-0.9,2-2V7.5c0.55,0,1-0.45,1-1C12,5.95,11.55,5.5,11,5.5z M9,9H7.25 C6.84,9,6.5,9.34,6.5,9.75c0,0.41,0.34,0.75,0.75,0.75H9V12H7.25c-0.41,0-0.75,0.34-0.75,0.75c0,0.41,0.34,0.75,0.75,0.75H9L9,15H5 V7.5h4V9z M19.5,10.5V10c0.55,0,1-0.45,1-1c0-0.55-0.45-1-1-1h-5c-0.55,0-1,0.45-1,1c0,0.55,0.45,1,1,1v0.5c0,0.5-1.5,1.16-1.5,3V20 c0,1.1,0.9,2,2,2h4c1.1,0,2-0.9,2-2v-6.5C21,11.66,19.5,11,19.5,10.5z M16.5,10.5V10h1v0.5c0,1.6,1.5,2,1.5,3V14h-4 c0-0.21,0-0.39,0-0.5C15,12.5,16.5,12.1,16.5,10.5z M15,20c0,0,0-0.63,0-1.5h4V20H15z" />
    </Icon>
  );
};

export const HealthAndSafetyIcon = ({ size, color, variant }) => {
  return (
    <Icon
      variant={variant}
      viewBox="0 0 24 24"
      fill="#000000"
      size={size}
      color={color}
    >
      <Rect fill="none" height="24" width="24" />
      <Path d="M10.5,13H8v-3h2.5V7.5h3V10H16v3h-2.5v2.5h-3V13z M12,2L4,5v6.09c0,5.05,3.41,9.76,8,10.91c4.59-1.15,8-5.86,8-10.91V5L12,2 z" />
    </Icon>
  );
};

export const MedicationLiquidIcon = ({ size, color, variant }) => {
  return (
    <Icon
      variant={variant}
      viewBox="0 0 24 24"
      fill="#000000"
      size={size}
      color={color}
    >
      <G>
        <Rect fill="none" height="24" width="24" />
      </G>
      <G>
        <G>
          <Rect height="2" width="12" x="3" y="3" />
          <Path d="M14,6H4C2.9,6,2,6.9,2,8v11c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V8C16,6.9,15.1,6,14,6z M13,15h-2.5v2.5h-3V15H5v-3h2.5 V9.5h3V12H13V15z" />
          <Path d="M20,6c-1.68,0-3,1.76-3,4c0,1.77,0.83,3.22,2,3.76V20c0,0.55,0.45,1,1,1s1-0.45,1-1v-6.24c1.17-0.54,2-1.99,2-3.76 C23,7.76,21.68,6,20,6z" />
        </G>
      </G>
    </Icon>
  );
};

export const AccountCircleOutline = ({ size, color, variant }) => {
  return (
    <Icon
      variant={variant}
      viewBox="0 0 24 24"
      fill="#000000"
      size={size}
      color={color}
    >
      <Path d="M0 0h24v24H0V0z" fill="none" />
      <Path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM7.07 18.28c.43-.9 3.05-1.78 4.93-1.78s4.51.88 4.93 1.78C15.57 19.36 13.86 20 12 20s-3.57-.64-4.93-1.72zm11.29-1.45c-1.43-1.74-4.9-2.33-6.36-2.33s-4.93.59-6.36 2.33C4.62 15.49 4 13.82 4 12c0-4.41 3.59-8 8-8s8 3.59 8 8c0 1.82-.62 3.49-1.64 4.83zM12 6c-1.94 0-3.5 1.56-3.5 3.5S10.06 13 12 13s3.5-1.56 3.5-3.5S13.94 6 12 6zm0 5c-.83 0-1.5-.67-1.5-1.5S11.17 8 12 8s1.5.67 1.5 1.5S12.83 11 12 11z" />
    </Icon>
  );
};
