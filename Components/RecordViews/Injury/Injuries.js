import React from "react";
import { Box, Text, HStack, Icon, Image, Pressable, VStack } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function Injury({ data: injury, color }) {
  const injuryDate = moment(injury.injured_at);
  const healedDate = moment(injury.healed_at);

  return (
    <Box>
      <HStack w="100%" alignItems="center" flex={1} bg="#FBFBFB">
        <VStack variant="recordEntryContainer">
          <HStack>
            <Icon color={color} as={<MaterialIcons name="healing" />} />
            <Text variant="recordEntryItem">{injury.name}</Text>
          </HStack>
          <HStack>
            <Icon color="#999" as={<MaterialIcons name="north-east" />} />
            <Text variant="recordEntryItem">
              Detectada el:{" "}
              {injuryDate.format("ddd").charAt(0).toUpperCase() +
                injuryDate.format("ddd").slice(1)}{" "}
              {injuryDate.format("D")} de {injuryDate.format("MMM YYYY")}
            </Text>
          </HStack>
          <HStack>
            <Icon color="#999" as={<MaterialIcons name="south-west" />} />
            <Text variant="recordEntryItem">
              Curada el:{" "}
              {injury.healed_at
                ? `${
                    healedDate.format("ddd").charAt(0).toUpperCase() +
                    healedDate.format("ddd").slice(1)
                  }
              ${healedDate.format("D")} de ${healedDate.format("MMM YYYY")}`
                : "Enfermedad vigente"}
            </Text>
          </HStack>
        </VStack>
      </HStack>
    </Box>
  );
}

// const injuries = {
//   extendable: false,
//   pressable: false,

//   firstField: {
//     icon: <MaterialIcons name="healing" />,
//     value: data.injury,
//   },
//   secondField: {
//     icon: <MaterialCommunityIcons name="flag-checkered" />,
//     value: new moment(data.injured_at).format("DD/MM/yyyy"),
//   },
//   thirdField: {
//     icon: <MaterialIcons name="do-not-disturb-on" />,
//     value: new moment(data.healed_at).format("DD/MM/yyyy"),
//   },
// };
