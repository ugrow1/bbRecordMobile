import {
  VStack,
  Text,
  Box,
  HStack,
  Icon,
  Center,
  Pressable,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import React from "react";
import RecordDetails from "../RecordDetails";
import { route, useScanDoctor } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function AppointmentDetails({
  route: {
    params: { color, appointment: appointment, babyId },
  },
  navigation,
}) {
  const appointmentDate = moment(appointment.date);
  const { onScannedDoctor } = useScanDoctor(babyId);

  return (
    <RecordDetails
      titleColor={color}
      title="Detalles de la cita"
      image={route + appointment.clinic.image.url}
      navigation={navigation}
    >
      <VStack flex={1}>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">FECHA DE LA CITA</Text>
            <Text>{`${appointmentDate.format("D")} de ${appointmentDate.format(
              "MMMM"
            )} del ${appointmentDate.format("YYYY")}`}</Text>
          </VStack>
        </Box>
        <Box variant="detailItem">
          <HStack justifyContent="space-between">
            <VStack variant="detailItemContent">
              <Text variant="detailItemHeading">HORA DE LA CITA</Text>
              <Text>{`${appointmentDate.format("h:MM A")}`}</Text>
            </VStack>
            <VStack variant="detailItemContent" mr={6}>
              <Text variant="detailItemHeading">DURACIÓN PROMEDIO</Text>
              <Text>30 Minutos</Text>
            </VStack>
          </HStack>
        </Box>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">LUGAR DE LA CITA</Text>
            <Text>{appointment.clinic.name}</Text>
          </VStack>
        </Box>
        <Box variant="detailItem">
          <HStack justifyContent="space-between">
            <VStack variant="detailItemContent">
              <Text variant="detailItemHeading">DIRECCIÓN DE LA CITA</Text>
              <Text>
                {appointment.clinic.adress}, {appointment.clinic.sector}
              </Text>
            </VStack>
            <Icon
              alignSelf="flex-end"
              size="sm"
              as={<MaterialIcons name="map" />}
              mr={6}
            />
          </HStack>
        </Box>
      </VStack>
      <Center mb={6}>
        <Pressable
          onPress={() => {
            navigation.navigate("Scan", {
              onScan: onScannedDoctor,
            });
          }}
          justifyContent="center"
          rounded={10}
          size="50px"
          bg={color}
          w="92%"
        >
          <HStack
            flex={1}
            space={4}
            justifyContent="center"
            alignItems="center"
          >
            <Icon
              position="absolute"
              left={2}
              color="#ffffff"
              size="28px"
              as={<MaterialIcons name="qr-code-2" />}
            />
            <Text color="#ffffff" fontSize="lg" bold>
              Iniciar la cita ahora
            </Text>
          </HStack>
        </Pressable>
      </Center>
    </RecordDetails>
  );
}
