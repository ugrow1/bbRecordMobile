import React from "react";
import { Box, Text, HStack, Icon, Image, Pressable } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function Appointment({
  data: appointment,
  navigation,
  jwt,
  color,
  babyId,
}) {
  const appointmentDate = moment(appointment.date);
  return (
    <Pressable
      onPress={() => {
        navigation.navigate("AppointmentDetails", {
          babyId: babyId,
          color: color,
          appointment: appointment,
        });
      }}
    >
      <HStack w="100%" alignItems="center" flex={1} bg="#FBFBFB">
        <Box flex={2}>
          <Text flex={1} fontSize="lg" bold pl={1} mb={4}>
            {appointmentDate.format("ddd").charAt(0).toUpperCase() +
              appointmentDate.format("ddd").slice(1)}{" "}
            {appointmentDate.format("D")} de{" "}
            {appointmentDate.format("MMMM, YYYY")}
          </Text>
          <HStack>
            <Icon size="sm" as={<MaterialIcons name="person" />} />
            <Text pl={1}>
              Dr. {appointment.doctor.first_name} {appointment.doctor.last_name}
            </Text>
          </HStack>
          <HStack>
            <Icon size="sm" as={<MaterialIcons name="place" />} />
            <Text pl={1}>{appointment.clinic.name}</Text>
          </HStack>
        </Box>
        <Box flex={1} BG="#000000" W="100%">
          <Image
            source={{
              uri: route + appointment.clinic.image.url,
            }}
            alt="Alternate Text"
            rounded="md"
            size="full"
          />
        </Box>
      </HStack>
    </Pressable>
  );
}
