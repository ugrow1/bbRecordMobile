import React from "react";
import { Box, Text, HStack, Icon, Image, Pressable, VStack } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function SufferedDiseases({ data: sufferedDisease, color }) {
  const diseaseFinished = new moment(sufferedDisease.finished_at);
  const diseaseStarted = new moment(sufferedDisease.finished_at);

  return (
    <Box>
      <HStack w="100%" alignItems="center" flex={1} bg="#FBFBFB">
        <VStack variant="recordEntryContainer">
          <HStack>
            <Icon color={color} as={<MaterialIcons name="coronavirus" />} />
            <Text variant="recordEntryItem">
              Padecimiento: {sufferedDisease.disease.name}
            </Text>
          </HStack>
          <HStack>
            <Icon color="#999" as={<MaterialIcons name="north-east" />} />
            <Text variant="recordEntryItem">
              Detectado el:{" "}
              {diseaseStarted.format("ddd").charAt(0).toUpperCase() +
                diseaseStarted.format("ddd").slice(1)}{" "}
              {diseaseStarted.format("D")} de{" "}
              {diseaseStarted.format("MMM YYYY")}
            </Text>
          </HStack>
          <HStack>
            <Icon color="#999" as={<MaterialIcons name="south-west" />} />
            <Text variant="recordEntryItem">
              Curado el:{" "}
              {diseaseFinished.format("ddd").charAt(0).toUpperCase() +
                diseaseFinished.format("ddd").slice(1)}{" "}
              {diseaseFinished.format("D")} de{" "}
              {diseaseFinished.format("MMM YYYY")}
            </Text>
          </HStack>
        </VStack>
      </HStack>
    </Box>
  );
}
