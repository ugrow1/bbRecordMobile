import React from "react";
import { Box, Text, HStack, Icon, Image, Pressable, VStack } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function Vaccine({
  data: vaccine,
  navigation,
  jwt,
  color,
  babyId,
}) {
  return (
    <Pressable
      onPress={() => {
        navigation.navigate("VaccineDetails", {
          babyId: babyId,
          color: color,
          vaccine: vaccine,
          jwt: jwt,
        });
      }}
    >
      <HStack w="100%" alignItems="center" flex={1} bg="#FBFBFB">
        <VStack variant="recordEntryContainer">
          <HStack>
            <Icon color={color} as={<MaterialIcons name="assignment" />} />
            <Text variant="recordEntryItem">{vaccine.name}</Text>
          </HStack>
          <HStack>
            <Icon color="#999" as={<MaterialIcons name="place" />} />
            <Text variant="recordEntryItem">{`Requeridas: ${vaccine.doses_required}`}</Text>
          </HStack>
        </VStack>
      </HStack>
    </Pressable>
  );
}
