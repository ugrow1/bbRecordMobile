import {
  VStack,
  Text,
  Box,
  HStack,
  Icon,
  Center,
  Pressable,
  Flex,
  TextArea,
  Checkbox,
  ScrollView,
  Input,
} from "native-base";
import axios from "axios";
import { StyleSheet, Modal, Platform } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import React from "react";
import RecordDetails from "../RecordDetails";
import { route, useScanDoctor } from "../../../Hooks";
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import "moment/locale/es";

export default function VaccineDetails({
  route: {
    params: { color, vaccine, babyId, jwt },
  },
  navigation,
}) {
  const [injectedDoses, setInjectedDoses] = React.useState([]);
  const [removedDoses, setRemovedDoses] = React.useState([]);
  const [addedDoses, setAddedDoses] = React.useState([]);
  const [doseAdded, setDoseAdded] = React.useState(false);
  const [checkedValues, setCheckedValues] = React.useState([]);
  const [requestSuccess, setRequestSuccess] = React.useState();

  React.useEffect(() => {
    async function getInjectionInfo() {
      //GET THE DATA OF THE INJECTION OF THAT VACCINE TO THE BABY
      const response = await axios.get(
        route + `/injected-vaccines?baby.id=${babyId}&vaccine.id=${vaccine.id}`,
        { headers: { Authorization: `Bearer ${jwt}` } }
      );

      const injectedVaccine = response.data;
      //IF THE REQUEST HAS RETURNED A VACCINE, THAT MEANS THE BABY HAS BEEN INJECTED WITH AT LEAST 1 DOSE
      if (injectedVaccine) {
        setInjectedDoses(injectedVaccine);
        modifyCheckedValues(injectedVaccine.length);
      }
    }

    getInjectionInfo();
  }, []);
  const onSubmit = async () => {
    //IF DOSES HAS BEEN ADDED
    try {
      if (addedDoses.length > 0) {
        const response = await axios.post(
          route + "/injected-vaccines/multiple",
          addedDoses,
          {
            headers: { Authorization: `Bearer ${jwt}` },
          }
        );
      }

      if (removedDoses.length > 0) {
        const response = await axios.delete(
          route + "/injected-vaccines/multiple/" + JSON.stringify(removedDoses),
          {
            headers: { Authorization: `Bearer ${jwt}` },
          }
        );
      }
      setRequestSuccess(true);
    } catch (error) {
      setRequestSuccess(false);
      console.log(error);
    }
  };

  const DatePicker = () => {
    return (
      <DateTimePicker
        is24Hour={true}
        display={Platform.OS === "ios" ? "inline" : "default"}
        value={new Date()}
        onChange={(event, date) => {
          //SET DOSE ADDED TO FALSE IMMEDIATLY A VALUE IS ENTERED TO CLOSE THE DATE PICKER ON TIME
          setDoseAdded(false);

          const newDoseAdded = {
            baby: babyId,
            vaccine: vaccine.id,
            injected_dose: doseAdded,
            injected_at: date,
          };

          var injectedDosesTMP = injectedDoses;
          var addedDosesTMP = addedDoses;

          if (event.type != "dismissed") {
            injectedDosesTMP.push(newDoseAdded);
            addedDosesTMP.push(newDoseAdded);
            setAddedDoses(addedDosesTMP);
            setInjectedDoses(injectedDosesTMP);
            modifyCheckedValues(injectedDoses.length);
          }

          return;
        }}
      />
    );
  };

  const removeInjected = () => {
    //IF THE DOSE REMOVED WAS ON THE DATABASE, IT HAS AN ID, SO WE ADD IT TO THE "removedDoses" VARIABLE
    if (injectedDoses[injectedDoses.length - 1].id) {
      //TEMPORAL VARIABLE TO INSERT THE ID OF THE DOSE TO BE DELETED
      const doseRemovedID = injectedDoses[injectedDoses.length - 1].id;
      const removedDosesTMP = removedDoses;
      removedDosesTMP.push(doseRemovedID);
      setRemovedDoses(removedDosesTMP);
    } else {
      const addedDosesTMP = addedDoses;
      addedDosesTMP.pop();
      setAddedDoses(addedDosesTMP);
    }

    var injectedDosesTMP = injectedDoses;
    injectedDosesTMP.pop();

    setInjectedDoses(injectedDosesTMP);

    modifyCheckedValues(injectedDoses.length);

    return;
  };

  const handleCheckBoxChange = (values) => {
    /**
     * @param values contains an array with the checkboxes checked: [1, 2, 3]
     *
     */

    if (values.length < injectedDoses.length) {
      removeInjected();
    } else {
      const addedValue = values.length;
      //UPDATES DOSE ADDED TO OPEN THE DATEPICKER
      setDoseAdded(addedValue);
    }
  };

  const modifyCheckedValues = (dosesInjected) => {
    /**
     * @param dosesInjected it's a number that indicates the amount of doses the child has ben injected with
     */

    var checkedValuesTMP = [];
    for (let index = 1; index <= dosesInjected; index++) {
      checkedValuesTMP.push(index);
    }

    setCheckedValues(checkedValuesTMP);
  };

  //RENDERS THE AMOUNT OF CHECKBOXES BASED ON THE AMOUNT OF DOSES REQUIRED
  var checkBoxes = [];
  if (injectedDoses) {
    for (let index = 0; index < vaccine.doses_required; index++) {
      const doseNumber = index + 1; //NUMBER OF THE DOSE OF THE CURRENT ITERATION
      var dateInjected = "";
      var formatedDate = "";
      if (injectedDoses[index]) {
        dateInjected = moment(injectedDoses[index].injected_at); //DATE THE DOSE WAS INJECTED
        formatedDate = `${
          dateInjected.format("dddd").charAt(0).toUpperCase() +
          dateInjected.format("dddd").slice(1)
        } ${dateInjected.format("D")} de ${dateInjected.format("MMMM, YYYY")}`;
      }

      checkBoxes.push(
        <Box
          key={doseNumber}
          variant="detailItem"
          w="100%"
          alignItems="flex-start"
        >
          <VStack variant="detailItemContent" alignItems="flex-start">
            <Text variant="detailItemHeading">{doseOrder[index]} DOSIS</Text>
            <Checkbox
              colorScheme="red"
              size="40px"
              w="100%"
              value={doseNumber}
              isDisabled={
                checkedValues.length != index &&
                checkedValues.length - 1 != index
              }
            >
              <Input
                ml={4}
                h="40px"
                w="85%"
                isDisabled
                value={formatedDate}
              ></Input>
            </Checkbox>
          </VStack>
        </Box>
      );
    }
  }
  return (
    <RecordDetails
      titleColor={color}
      title="Detalles de la vacuna"
      navigation={navigation}
    >
      <VStack flex={1}>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">VACUNA</Text>
            <Text> {vaccine.name}</Text>
          </VStack>
        </Box>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">DOSIS REQUERIDAS</Text>
            <Text>{vaccine.doses_required} dosis</Text>
          </VStack>
        </Box>
        {/* DATE PICKER IN CASE IT IS AN ANDROID */}
        {Platform.OS == "ios" ? (
          <Modal
            animationType="fade"
            transparent
            visible={doseAdded !== false}
            presentationStyle="overFullScreen"
          >
            <Flex bg="rgba(0,0,0,.2)" flex={1} justify="center">
              <View bg="white">
                <DatePicker />
              </View>
            </Flex>
          </Modal>
        ) : (
          doseAdded !== false && <DatePicker />
        )}
        <Checkbox.Group
          value={checkedValues}
          accessibilityLabel="pick an item"
          onChange={handleCheckBoxChange}
        >
          {checkBoxes}
        </Checkbox.Group>
      </VStack>

      <Center mb={6}>
        {requestSuccess == true && (
          <Pressable
            alignItems="center"
            justifyContent="center"
            rounded={10}
            size="50px"
            bg="#0F9D58"
            w="92%"
          >
            <Text color="#ffffff" fontSize="lg" bold>
              Cambios guardados!
            </Text>
          </Pressable>
        )}
        {requestSuccess == false && (
          <Pressable
            alignItems="center"
            justifyContent="center"
            rounded={10}
            size="50px"
            bg="#BE0000"
            w="92%"
          >
            <Text color="#ffffff" fontSize="lg" bold>
              Ha ocurrido un error!
            </Text>
          </Pressable>
        )}

        <Pressable
          onPress={onSubmit}
          justifyContent="center"
          rounded={10}
          size="50px"
          bg={color}
          w="92%"
        >
          <HStack
            flex={1}
            space={4}
            justifyContent="center"
            alignItems="center"
          >
            {/* <Icon
              position="absolute"
              left={2}
              color="#ffffff"
              size="28px"
              as={<MaterialIcons name="qr-code-2" />}
            /> */}
            <Text color="#ffffff" fontSize="lg" bold>
              Guardar cambios
            </Text>
          </HStack>
        </Pressable>
      </Center>
    </RecordDetails>
  );
}

const doseOrder = [
  "PRIMERA",
  "SEGUNDA",
  "TERCERA",
  "CUARTA",
  "QUINTA",
  "SEXTA",
  "SÉPTIMA",
  "NOVENA",
  "DÉCIMA",
];
