import React from "react";
import { Box, Text, HStack, Icon, Image, Pressable } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function Appointment({
  data: prescription,
  navigation,
  jwt,
  color,
  babyId,
}) {
  const prescriptionDate = moment(prescription.date);
  return (
    <Pressable
      onPress={() => {
        navigation.navigate("PrescriptionDetails", {
          babyId: babyId,
          color: color,
          prescription: prescription,
        });
      }}
    >
      <HStack w="100%" alignItems="center" flex={1} bg="#FBFBFB">
        <Box flex={2}>
          <Text flex={1} fontSize="lg" bold pl={1} mb={4}>
            {prescriptionDate.format("ddd").charAt(0).toUpperCase() +
              prescriptionDate.format("ddd").slice(1)}{" "}
            {prescriptionDate.format("D")} de{" "}
            {prescriptionDate.format("MMMM, YYYY")}
          </Text>
          <HStack>
            <Icon size="sm" as={<MaterialIcons name="person" />} />
            <Text pl={1}>
              Dr. {prescription.doctor.first_name}{" "}
              {prescription.doctor.last_name}
            </Text>
          </HStack>
          <HStack>
            <Icon size="sm" as={<MaterialIcons name="place" />} />
            <Text pl={1}>{prescription.clinic.name}</Text>
          </HStack>
        </Box>
        <Box flex={1} BG="#000000" W="100%">
          <Image
            source={{
              uri: route + prescription.image[0].url,
            }}
            alt="Alternate Text"
            rounded="md"
            size="full"
          />
        </Box>
      </HStack>
    </Pressable>
  );
}

// const prescriptions = {
//   extendable: true,
//   pressable: true,
//   image: data.image[0].url,
//   firstField: {
//     field: "Doctor",
//     icon: <MaterialIcons name="person" />,
//     value: data.doctor.first_name,
//   },
//   secondField: {
//     field: "Clínica",
//     icon: <MaterialIcons name="place" />,
//     value: data.clinic.name,
//   },
//   thirdField: {
//     field: "Fecha",
//     icon: <MaterialIcons name="schedule" />,
//     value: new moment(data.date).format("DD/MM/yyyy - hh:mm A"),
//   },
//   details: [
//     { field: "Código QR", image: data.bar_code.url },
//     { field: "Mensaje", value: data.message.prescripcion },
//   ],
// };
