import {
  VStack,
  Text,
  Box,
  HStack,
  Icon,
  Center,
  Pressable,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import React from "react";
import RecordDetails from "../RecordDetails";
import { route, useScanDoctor } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function PrescriptionDetails({
  route: {
    params: { color, prescription, babyId },
  },
  navigation,
}) {
  const prescriptionDate = moment(prescription.date);
  const prescriptionMessage = prescription.message.map(
    (recipe) => `\n${recipe.medicine}: ${recipe.dose}\n`
  );
  return (
    <RecordDetails
      titleColor={color}
      title="Detalles de la prescripción"
      image={route + prescription.image[0].url}
      navigation={navigation}
    >
      <VStack flex={1}>
        <Box variant="detailItem">
          <HStack justifyContent="space-between">
            <VStack variant="detailItemContent">
              <Text variant="detailItemHeading">RECETADO POR</Text>
              <Text>{`Dr. ${prescription.doctor.first_name}`}</Text>
            </VStack>
            <VStack variant="detailItemContent">
              <Text variant="detailItemHeading">EN LA CLÍNICA</Text>
              <Text>{prescription.clinic.name}</Text>
            </VStack>
          </HStack>
        </Box>
        <Box variant="detailItem">
          <VStack variant="detailItemContent" mr={6}>
            <Text variant="detailItemHeading">DESCRIPCIÓN</Text>
            <Text>{prescriptionMessage}</Text>
          </VStack>
        </Box>

        <Box variant="detailItem">
          <HStack justifyContent="space-between">
            <VStack variant="detailItemContent">
              <Text variant="detailItemHeading">FECHA DE EMISIÓN</Text>
              <Text>
                {`${prescriptionDate.format("D")} de ${prescriptionDate.format(
                  "MMMM"
                )} del ${prescriptionDate.format("YYYY")}`}
              </Text>
            </VStack>
          </HStack>
        </Box>
      </VStack>
    </RecordDetails>
  );
}
