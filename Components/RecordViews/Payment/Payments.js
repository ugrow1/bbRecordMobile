const payments = {
  extendable: false,
  pressable: false,

  firstField: {
    icon: <MaterialIcons name="article" />,
    value: data.purpose,
  },
  secondField: {
    icon: <MaterialIcons name="attach-money" />,
    value: data.amount,
  },
  thirdField: {
    icon: <MaterialIcons name="person" />,
    value: "Dr. " + data.doctor.first_name + " " + data.doctor.last_name,
  },
};
