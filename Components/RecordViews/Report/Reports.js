import React from "react";
import { Box, Text, HStack, Icon, Image, Pressable } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function Report({
  data: report,
  navigation,
  jwt,
  color,
  babyId,
}) {
  const dateEmited = moment(report.created_at);

  return (
    <Pressable
      onPress={() => {
        navigation.navigate("ReportDetails", {
          babyId: babyId,
          color: color,
          report: report,
        });
      }}
    >
      <HStack w="100%" alignItems="center" flex={1} bg="#FBFBFB">
        <Box flex={2}>
          <Text flex={1} fontSize="lg" bold pl={1} mb={4}>
            {dateEmited.format("ddd").charAt(0).toUpperCase() +
              dateEmited.format("ddd").slice(1)}{" "}
            {dateEmited.format("D")} de {dateEmited.format("MMMM, YYYY")}
          </Text>
          <HStack>
            <Icon size="sm" as={<MaterialIcons name="article" />} />
            <Text pl={1}>{report.title}</Text>
          </HStack>
        </Box>
        {report.image && (
          <Box flex={1} BG="#000000" W="100%">
            <Image
              source={{
                uri: route + report.image.url,
              }}
              alt="Alternate Text"
              rounded="md"
              size="full"
            />
          </Box>
        )}
      </HStack>
    </Pressable>
  );
}

// const reports = {
//   extendable: true,
//   pressable: true,
//   date_emited: new moment(data.date).format("dddd, MM yyyy - hh:mm A"),
//   image: data.image ? data.image.url : undefined,
//   firstField: {
//     field: "Título",
//     icon: <MaterialIcons name="article" />,
//     value: data.title,
//   },
//   details: [
//     { field: "Mensaje", value: data.message },

//     {
//       field: "Enviado el: ",
//       value: new moment(data.date).format("DD/MM/yyyy - hh:mm A"),
//     },
//   ],
// };
