import {
  VStack,
  Text,
  Box,
  HStack,
  Icon,
  Center,
  Pressable,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import React from "react";
import RecordDetails from "../RecordDetails";
import { route, useScanDoctor } from "../../../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function AppointmentDetails({
  route: {
    params: { color, report, babyId },
  },
  navigation,
}) {
  const dateEmited = moment(report.created_at);

  return (
    <RecordDetails
      titleColor={color}
      title="Detalles de la alerta"
      image={report.image && route + report.image.url}
      navigation={navigation}
    >
      <VStack flex={1}>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">ASUNTO</Text>
            <Text>{report.title}</Text>
          </VStack>
        </Box>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">MENSAJE</Text>
            <Text>{report.message}</Text>
          </VStack>
        </Box>
        <Box variant="detailItem">
          <HStack justifyContent="space-between">
            <VStack variant="detailItemContent">
              <Text variant="detailItemHeading">ENVIADO EL</Text>
              <Text>
                {dateEmited.format("ddd").charAt(0).toUpperCase() +
                  dateEmited.format("ddd").slice(1)}{" "}
                {dateEmited.format("D")} de {dateEmited.format("MMMM, YYYY")}
              </Text>
            </VStack>
          </HStack>
        </Box>
      </VStack>
    </RecordDetails>
  );
}
