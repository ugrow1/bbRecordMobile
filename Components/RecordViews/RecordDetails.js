import React from "react";
import { Text, HStack, Icon, VStack, Image, Box } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";

export default function RecordDetails({
  children,
  image,
  title,
  titleColor,
  navigation,
}) {
  return (
    <VStack flex={1} bg="#ffffff">
      <Box px={5} position="absolute" w="100%" top={5}>
        <HStack flex={1} w="100%" justifyContent="center" alignItems="center">
          <Icon
            size="sm"
            left={0}
            position="absolute"
            as={<MaterialIcons name="arrow-back-ios" />}
            onPress={navigation.goBack}
          />
          <Text color={titleColor} fontSize="22px">
            {title}
          </Text>
        </HStack>
      </Box>
      <Box flex={1} mt="75px">
        {image && (
          <Box mx={5} mb={5} flex={4} shadow={3} rounded={18}>
            <Image
              source={{ uri: image }}
              alt="image"
              rounded={18}
              size="full"
            />
          </Box>
        )}

        <Box flex={10}>{children}</Box>
      </Box>
    </VStack>
  );
}
