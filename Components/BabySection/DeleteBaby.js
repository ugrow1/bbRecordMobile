import React from "react";
import { Modal, Button, Text } from "native-base";
import axios from "axios";
import { route, socket } from "../../Hooks";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function deleteBaby({ babyId, setDeletingBaby, jwt }) {
  const deleteBaby = async () => {
    await axios.put(
      route + "/babies/" + babyId,
      { deleted: true, deleted_at: new Date() },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    );
    await AsyncStorage.setItem("activeBabyId", "");
    socket.emit("babyUpdated", socket.id);
    return;
  };
  return (
    <Modal isOpen={true} onClose={() => {}}>
      <Modal.Content>
        <Modal.Body>
          <Text fontSize="lg">
            Eliminar este bebé es una acción permanente. Está seguro de que
            desea eliminarlo?
          </Text>
        </Modal.Body>
        <Modal.Footer>
          <Button.Group variant="ghost" space={2}>
            <Button
              onPress={() => {
                setDeletingBaby(false);
              }}
            >
              Cancelar
            </Button>
            <Button
              onPress={() => {
                deleteBaby();
                setDeletingBaby(false);
              }}
            >
              Eliminar
            </Button>
          </Button.Group>
        </Modal.Footer>
      </Modal.Content>
    </Modal>
  );
}
