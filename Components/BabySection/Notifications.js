import React, { useState, useEffect } from "react";
import { Divider, VStack, Box, Text } from "native-base";
import { route } from "../../Hooks";
import axios from "axios";
import moment from "moment";

export default function Notifications({ baby }) {
  const [data, setData] = useState();
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  useEffect(() => {
    async function getNotifications() {
      const response = await axios.get(route + "/reports?baby.id=" + baby.id, {
        cancelToken: source.token,
      });

      setData(response.data);
    }
    getNotifications();
  }, []);

  if (data) {
    const todayDate = new moment().format("YYYY/MM/DD");
    const entries = data.map(
      (notification) =>
        new moment(notification.date).format("YYYY/MM/DD") == todayDate && (
          <Box key={notification.id} px={4}>
            <Text>{notification.title}</Text>
          </Box>
        )
    );
    return (
      <VStack space={4} divider={<Divider />} pb={4}>
        <Box px={4} pt={4}>
          <Text bold fontSize="lg">
            Notificaciones de hoy
          </Text>
        </Box>
        {entries}
      </VStack>
    );
  }
  return null;
}
