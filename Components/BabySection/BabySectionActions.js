import {
  Text,
  Icon,
  HStack,
  VStack,
  Box,
  IconButton,
  Center,
} from "native-base";
import { CurrentBabyContext } from "../../Hooks";
import React, { useContext } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { StyleSheet, Dimensions } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  MedicationLiquidIcon,
  HealthAndSafetyIcon,
  VaccinesIcon,
} from "../CustomIcons";
export default function ActionButtons({ styles, currentBaby }) {
  const navigation = useNavigation();
  return (
    <VStack space={5}>
      <HStack justifyContent="space-between">
        <VStack space={3} alignItems="center">
          <IconButton
            variant="actionButton"
            onPress={() =>
              navigation.navigate("QuickActionsView", {
                color: "#BE009E",
                view: "appointments",
                query:
                  "_sort=date:ASC&_where[_or][0][state_ne]=Finalizada&_where[_or][1][state_ne]=Cancelada&date_gte=" +
                  new Date().toISOString(),
                viewTitle: "Citas",
                currentBaby: currentBaby,
              })
            }
            icon={
              <Icon
                size="36px"
                variant="actionIcon"
                color="#BE009E"
                as={<MaterialIcons name="event-note" />}
              />
            }
          />
          <Text>Citas</Text>
        </VStack>
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("BabyDoctors", {
                color: "#6600BE",
                babyName: currentBaby.name,
                babyImage: currentBaby.image,
                babyId: currentBaby.id,
              })
            }
            variant="actionButton"
            icon={
              <Icon
                size="36px"
                variant="actionIcon"
                color="#6600BE"
                as={<MaterialIcons name="medical-services" />}
              />
            }
          />
          <Text>Doctores</Text>
        </VStack>
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("QuickActionsView", {
                color: "#006CBE",
                view: "suffered-diseases",
                query: "_sort=created_at:DESC",
                viewTitle: "Salud",
                currentBaby: currentBaby,
              })
            }
            variant="actionButton"
            icon={
              <HealthAndSafetyIcon
                size="36px"
                variant="actionIcon"
                color="#006CBE"
              />
            }
          />

          <Text>Salud</Text>
        </VStack>
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("QuickActionsView", {
                color: "#BE8D00",

                view: "reports",
                viewTitle: "Alertas",
                query: "report_type.name=Alerta&_sort=created_at:DESC",
                currentBaby: currentBaby,
              })
            }
            variant="actionButton"
            icon={
              <Icon
                size="36px"
                variant="actionIcon"
                color="#BE8D00"
                as={<MaterialIcons name="report" />}
              />
            }
          />

          <Text>Alertas</Text>
        </VStack>
      </HStack>
      <HStack justifyContent="space-between">
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("QuickActionsView", {
                color: "#0057BE",
                query: "_sort=created_at:DESC",
                view: "injuries",
                viewTitle: "Lesiones",
                currentBaby: currentBaby,
              })
            }
            variant="actionButton"
            icon={
              <Icon
                size="36px"
                variant="actionIcon"
                color="#0057BE"
                as={<MaterialIcons name="healing" />}
              />
            }
          />
          <Text>Lesiones</Text>
        </VStack>
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("QuickActionsView", {
                color: "#00B5BE",
                view: "prescriptions",
                query: "_sort=created_at:DESC",
                viewTitle: "Prescripciones",
                currentBaby: currentBaby,
              })
            }
            variant="actionButton"
            icon={
              <MedicationLiquidIcon
                size="36px"
                variant="actionIcon"
                color="#00B5BE"
              />
            }
          />
          <Text>Recetas</Text>
        </VStack>
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("QuickActionsView", {
                color: "#BE0000",
                view: "vaccines",
                viewTitle: "Vacunas",
                currentBaby: currentBaby,
              })
            }
            variant="actionButton"
            icon={
              <VaccinesIcon size="36px" variant="actionIcon" color="#BE0000" />
            }
          />
          <Text>Vacunas</Text>
        </VStack>
        <VStack space={3} alignItems="center">
          <IconButton
            onPress={() =>
              navigation.navigate("BabyRecord", {
                currentBaby: currentBaby,
              })
            }
            variant="actionButton"
            icon={
              <Icon
                size="36px"
                variant="actionIcon"
                color="#00BD21"
                as={<MaterialIcons name="app-registration" />}
              />
            }
          />
          <Text>Récord</Text>
        </VStack>
      </HStack>
    </VStack>
  );
}
