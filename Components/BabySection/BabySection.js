import {
  IconButton,
  Button,
  Box,
  Pressable,
  Icon,
  HStack,
  Heading,
  Text,
  Modal,
} from "native-base";

import DeleteBaby from "./DeleteBaby";
import React, { useEffect, useState } from "react";
import BabySectionActions from "./BabySectionActions";
import { useIsFocused, useNavigation } from "@react-navigation/native";
import { MaterialIcons, AntDesign } from "@expo/vector-icons";
import useScanDoctor from "../../Hooks/useScanDoctor";
import moment from "moment";
import axios from "axios";

export default function BabySection({ baby, setDoctors, jwt }) {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  useEffect(() => {
    if (isFocused) {
      setDoctors(baby.id);
    }
  }, [isFocused]);
  const { onScannedDoctor } = useScanDoctor(baby.id);
  const birth_date = new moment(baby.birth_date);
  const today = new moment();
  const monthsAlive = today.diff(birth_date, "months");
  const daysAlive = Math.abs(birth_date.format("D") - today.format("D"));
  const [deletingBaby, setDeletingBaby] = useState(false);

  return (
    <Box flex={5} bg="#ffffff">
      <Box alignItems="center" flex={1} mx={6} mb={8}>
        {/* Center Box */}
        {deletingBaby && (
          <DeleteBaby
            babyId={baby.id}
            setDeletingBaby={setDeletingBaby}
            jwt={jwt}
          />
        )}
        <Box
          bg="#0484DB"
          shadow={9}
          shadowColor="#0484DB"
          size="44%"
          w="100%"
          borderRadius={20}
        >
          <Box position="absolute" w="100%" bottom={0} p={4}>
            {/* NAME AND EDIT BUTTON */}
            <HStack alignItems="center">
              <Heading color="#FFFFFF" fontSize="xl">
                {(baby.name + " " + baby.last_name).toUpperCase()}
              </Heading>
              <IconButton
                alignItems="center"
                onPress={() =>
                  navigation.navigate("RegisterBaby", {
                    baby: baby,
                    modify: true,
                  })
                }
                size="md"
                icon={
                  <Icon
                    size="sm"
                    color="#ffffff"
                    as={<MaterialIcons name="edit" />}
                  />
                }
              />
            </HStack>
            {/* TIME ALIVE AND DELETE BUTTON */}
            <HStack justifyContent="space-between" alignItems="center">
              <Text alignContent="flex-start" color="#FFFFFF">
                {monthsAlive} meses, {daysAlive} días
              </Text>
              <IconButton
                onPress={() => setDeletingBaby(true)}
                size="xs"
                icon={
                  <Icon
                    size="sm"
                    color="#ffffff"
                    as={<MaterialIcons name="delete" />}
                  />
                }
              />
            </HStack>
          </Box>
        </Box>
        <Box mt={7} mb={5} w="100%">
          <BabySectionActions currentBaby={baby} />
        </Box>

        <Pressable
          onPress={() => {
            navigation.navigate("Scan", {
              onScan: onScannedDoctor,
            });
          }}
          justifyContent="center"
          rounded={18}
          size="65px"
          bg="#F2F5FA"
          w="100%"
        >
          <HStack space={4} justifyContent="center" alignItems="center">
            <Icon
              color="#3F4951"
              size="34px"
              as={<MaterialIcons name="qr-code-scanner" />}
            />
            <Text bold>Compartir record con tu doctor</Text>
            <Icon size="sm" as={<MaterialIcons name="arrow-drop-down" />} />
          </HStack>
        </Pressable>
      </Box>
    </Box>
  );
}
