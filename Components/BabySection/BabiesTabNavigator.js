import BabySection from "./BabySection";
import React, { useContext, useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  Text,
  Image,
  Box,
  Center,
  ScrollView,
  Pressable,
  HStack,
  Icon,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import NoRegisteredEntry from "../../Views/NoRegisteredEntry";
import { useNavigation } from "@react-navigation/native";
import { AuthUserContext, route, socket } from "../../Hooks";
import { StyleSheet } from "react-native";
import Feed from "../../Views/Feed";
import { SplitscreenIcon } from "../CustomIcons";
import axios from "axios";

//this component renders a tab navigator wich displays the user babies
export default function BabiesTabNavigator(props) {
  const [babies, setBabies] = useState([]);
  const [activeBaby, setActiveBaby] = useState(0);
  const [loading, setLoading] = useState(true);
  const [showFeed, setShowFeed] = useState(false);
  const navigation = useNavigation();

  //we use the context being passed on app.js wich has the authenticated user
  const {
    userData: { user, jwt },
  } = useContext(AuthUserContext);

  const getBabies = async () => {
    const activeBabyId = await AsyncStorage.getItem("activeBabyId");

    const response = await axios.get(
      route + `/babies?creator=${user.id}&deleted=${false}`
    );
    const fetchedBabies = response.data;

    if (activeBabyId) {
      fetchedBabies.forEach((baby, index) => {
        if (baby.id == activeBabyId) {
          changeActiveBaby(index, activeBabyId);
        }
      });
    } else {
      changeActiveBaby(0, fetchedBabies.length == 0 ? "" : fetchedBabies[0].id);
    }

    setBabies(fetchedBabies);
    setLoading(false);
  };

  useEffect(() => {
    getBabies();

    socket.on("updateBaby", () => {
      getBabies();
    });

    return () => {
      socket.off("updateBaby");
    };
  }, []);

  const changeActiveBaby = async (babyIndex, babyId) => {
    if (showFeed) {
      setShowFeed(false);
    }
    setActiveBaby(babyIndex);
    if (babyId) {
      props.setDoctors(babyId);
    }
    await AsyncStorage.setItem("activeBabyId", babyId.toString());
  };

  //this function gets the babies of the auth user to display those babies screens
  if (loading) {
    return null;
  } else {
    return (
      <>
        {!babies[activeBaby] ? (
          <>
            {/* <Center flex={1} bg="#ffffff" borderWidth={1} borderColor="#f0f0f0">
              <Icon
                color="#0484DB"
                as={<MaterialIcons name="add-circle-outline" />}
                size={9}
              />
            </Center> */}

            <NoRegisteredEntry
              displayText="¡Aún no tiene bebés registrados!"
              displayIcon={<MaterialIcons name="child-care" />}
              navigateTo={{ route: "RegisterBaby", props: {} }}
            />
          </>
        ) : (
          <>
            {/* TAB NAVIGATOR */}
            <HStack
              bg="#ffffff"
              h="11.3%"
              mb={8}
              borderTopWidth={1}
              borderBottomWidth={1}
              borderColor="#f0f0f0"
            >
              <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                flex={1}
              >
                {babies.map((baby, index) => (
                  <Box key={baby.id}>
                    <Pressable
                      flex={1}
                      onPress={(e) => changeActiveBaby(index, baby.id)}
                      id="1"
                      alignItems="center"
                      justifyContent="center"
                      size="75px"
                    >
                      <Image
                        source={
                          baby.image
                            ? {
                                uri: route + baby.image?.url,
                              }
                            : require("../../assets/defaultUserImage.png")
                        }
                        alt="Alternate Text"
                        rounded="full"
                        size="42px"
                        mb={1}
                      />
                      <Text fontSize="xs" color="#ACACAC">
                        {baby.name.split(" ")[0]}
                      </Text>
                    </Pressable>
                    {activeBaby == index && !showFeed && (
                      <Box
                        pos="absolute"
                        top={0}
                        bottom={0}
                        left={0}
                        right={0}
                        justifyContent="flex-end"
                        alignItems="center"
                      >
                        <Box
                          bg={activeBaby == index ? "#509BFE" : "#ffffff"}
                          borderRadius="md"
                          w="25%"
                          size={1}
                        />
                      </Box>
                    )}
                  </Box>
                ))}
              </ScrollView>
              <Box justifyContent="center">
                <Pressable
                  onPress={() => {
                    setShowFeed(true);
                  }}
                  alignSelf="center"
                  justifyContent="center"
                  borderColor="#f0f0f0"
                  borderLeftWidth={1}
                  px="6"
                >
                  <Center bg="#F2F5FA" borderRadius="full" size="43px" mb={1}>
                    <SplitscreenIcon size={7} />
                  </Center>
                  <Center>
                    <Text color="#ACACAC" fontSize="xs">
                      Feed
                    </Text>
                  </Center>
                </Pressable>
                <Box
                  pos="absolute"
                  top={0}
                  bottom={0}
                  left={0}
                  right={0}
                  justifyContent="flex-end"
                  alignItems="center"
                >
                  <Box
                    bg={showFeed ? "#509BFE" : "#ffffff"}
                    borderRadius="md"
                    w="25%"
                    size={1}
                  />
                </Box>
              </Box>
            </HStack>

            {showFeed ? (
              <Feed navigation={navigation} />
            ) : (
              <BabySection
                baby={babies[activeBaby]}
                setDoctors={props.setDoctors}
                jwt={jwt}
              />
            )}
          </>
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  tabNavIcons: {},
});

BabiesTabNavigator.defaultProps = {
  route: {
    params: {
      updateBabies: false,
    },
  },
};
