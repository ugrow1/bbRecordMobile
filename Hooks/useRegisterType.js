import { route } from "./baseRoute";

export default function useRegisterType(registerType) {
  switch (registerType) {
    case "baby":
      const dateOfRegistration = new Date();
      const fieldsForBabyCreation = {
        viewTitle: "Registrar bebé",
        endpoint: route + "/babies",
        basicPersonInfo: true,
        successDisplayInfo: {
          image: true,
          name: "Nombre",
          last_name: "Apellido",
          birth_date: "Fecha de Nacimiento",
        },
        extraFields: {
          1: {
            fieldType: "number",
            fieldName: "Peso",
            fieldInDb: {
              name: "weight",
              value: { [dateOfRegistration]: "" },
            },
          },
          2: {
            fieldType: "number",
            fieldName: "Talla",
            fieldInDb: {
              name: "measures",
              value: { [dateOfRegistration]: "" },
            },
          },
          3: {
            fieldType: "number",
            fieldName: "Presión craneal",
            fieldInDb: {
              name: "cranial_pressure",
              value: { [dateOfRegistration]: "" },
            },
          },
        },
      };
      return fieldsForBabyCreation;

    case "user":
      const fieldsForUserCreation = {
        viewTitle: "Registrar usuario",
        endpoint: route + "/auth/local/register",
        basicPersonInfo: true,
        successDisplayInfo: {
          image: true,
          name: "Nombre",
          last_name: "Apellido",
          birth_date: "Fecha de Nacimiento",
        },
        extraFields: {
          1: {
            fieldType: "string",
            fieldName: "Correo electrónico",
            fieldInDb: {
              name: "email",
              value: "",
            },
          },
          2: {
            fieldType: "string",
            fieldName: "Contraseña",
            hidden: true,
            fieldInDb: {
              name: "password",
              value: "",
            },
          },
          3: {
            fieldType: "string",
            fieldName: "Confirmar contraseña",
            hidden: true,
            fieldInDb: {
              name: "passwordConfirmation",
              value: "",
            },
          },

          4: {
            fieldType: "string",
            fieldName: "Cédula",
            fieldInDb: {
              name: "id_card",
              value: "",
            },
          },
          5: {
            fieldType: "string",
            fieldName: "Teléfono",
            fieldInDb: {
              name: "phone",
              value: "",
            },
          },
          6: {
            fieldType: "string",
            fieldName: "País",
            fieldInDb: {
              name: "country",
              value: "",
            },
          },
          7: {
            fieldType: "string",
            fieldName: "Ciudad",
            fieldInDb: {
              name: "city",
              value: "",
            },
          },
          8: {
            fieldType: "string",
            fieldName: "Dirección",
            fieldInDb: {
              name: "address",
              value: "",
            },
          },
        },
      };
      return fieldsForUserCreation;
  }
}
