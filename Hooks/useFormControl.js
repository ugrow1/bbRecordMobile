import React, { useState } from "react";
import { Alert } from "native-base";

export default function useFormControl() {
  const [activeAlert, setActiveAlert] = useState();
  const [emptyFields, setEmptyFields] = useState({});
  const [passwordsConfirmed, setPasswordsConfirmed] = useState(true);

  const checkEmptyFields = (stateObject, exceptions = []) => {
    const emptyFieldsTMP = {};
    for (let [state, value] of Object.entries(stateObject)) {
      if (!exceptions.includes(state)) {
        if (value == "" || value == null) {
          emptyFieldsTMP[state] = true;
        }
      }
    }
    setEmptyFields(emptyFieldsTMP);
  };

  const arePasswordsEqual = (firstPassword, secondPassword) => {
    if (firstPassword !== secondPassword) {
      setPasswordsConfirmed(false);
      return false;
    } else {
      setPasswordsConfirmed(true);
      return true;
    }
  };

  const saveSuccess = () => {
    setActiveAlert("save_success");
  };

  const saveError = () => {
    setActiveAlert("save_error");
  };

  const DisplayAlert = () => {
    return (
      <Alert
        status={activeAlert == "save_success" ? "success" : "error"}
        w="100%"
      >
        <Alert.Icon />
        <Alert.Title>
          {activeAlert == "save_success" && "Cambios guardados correctamente"}
          {activeAlert == "save_error" &&
            "No se han podido guardar los cambios"}
          {activeAlert == "password_not_equal" &&
            "Las contraseñas no son iguales"}
          {activeAlert == "invalid_field" &&
            "Por favor llene todos los campos correctamente"}
        </Alert.Title>
      </Alert>
    );
  };

  return {
    checkEmptyFields,
    emptyFields,
    saveSuccess,
    DisplayAlert,
    arePasswordsEqual,
    passwordsConfirmed,
  };
}
