import { io } from "socket.io-client";
import { route } from "./baseRoute";
const socket = io(route);
export default socket;
