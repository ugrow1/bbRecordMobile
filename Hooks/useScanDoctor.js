import { useState, useContext } from "react";
import socket from "./socket";
import { route } from "./baseRoute";
import { AuthUserContext } from "./userContext";

import axios from "axios";

export default function useScanDoctor(babyId, fetchedDoctors) {
  const { userData } = useContext(AuthUserContext);
  const [doctors, setDoctors] = useState(fetchedDoctors);
  const onScannedDoctor = async (data) => {
    try {
      data = JSON.parse(data);
      const newDoctors = await addDoctorToBaby(data.doctorId);
      socket.emit("DoctorScanned", data.socketId, babyId);

      //RETURN NEW DOCTORS
      return newDoctors;
    } catch (error) {
      console.log(error);
    }
  };

  const addDoctorToBaby = async (doctorId) => {
    var babyRequest;
    //IF THE BABY DOCTORS WEREN'T PASSED, WE REQUEST THE BABY TO GET ITS DOCTORS
    if (!doctors) {
      babyRequest = await axios.get(route + "/babies/" + babyId, {
        headers: { Authorization: `Bearer ${userData.jwt}` },
      });
    }

    //SETS TEMPORAL VARIABLE WITH THE DOCTORS TO PUSH THE NEW DOCTOR IN THE ARRAY AND THEN UPDATE THE BABY
    const doctorsTMP =
      babyRequest !== undefined ? babyRequest.data.doctors : doctors;

    doctorsTMP.push(doctorId);

    const response = await axios.put(route + "/babies/" + babyId, {
      doctors: doctorsTMP,
    });

    return response.data.doctors;
  };

  return { onScannedDoctor, addDoctorToBaby, setFetchedDoctors: setDoctors };
}
