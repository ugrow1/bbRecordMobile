import axios from "axios";
import { route } from "./baseRoute";

export default function useSignIn() {
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  //gets the user token and information based on the FormControl data

  const logIn = async (identifier, password) => {
    const body = {
      identifier: identifier,
      password: password,
    };
    var response;

    try {
      response = await axios.post(route + "/auth/local", body, {
        CancelToken: source.token,
      });
    } catch (error) {
      console.log(error);
    }

    return response.data;
  };

  return logIn;
}
