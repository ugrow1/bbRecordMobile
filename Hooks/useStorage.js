import AsyncStorage from "@react-native-async-storage/async-storage";
import { useState } from "react";
export default function useStorage() {
  const [data, setData] = useState();
  const storeData = async (key, data) => {
    try {
      setData(data);
      const jsonUserData = JSON.stringify(data);
      await AsyncStorage.setItem(key, jsonUserData);
    } catch (e) {}

    return;
  };

  const removeData = async (key) => {
    try {
      await AsyncStorage.removeItem(key);
      setData(undefined);
    } catch (e) {
      // remove error
    }
  };

  // const retrieveData = async (key) => {
  //   try {
  //     const jsonUserData = await AsyncStorage.getItem(key);
  //     const AuthUser = jsonUserData != null ? JSON.parse(jsonUserData) : null;
  //     return AuthUser;
  //   } catch (e) {
  //     // error reading value
  //   }
  // };

  return [data, storeData, removeData];
}
