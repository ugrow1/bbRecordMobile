import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { NativeBaseProvider } from "native-base";
import { useFonts } from "expo-font";

import { theme } from "./Theme";
import { useStorage, AuthUserContext } from "./Hooks";

import {
  QuickActionsView,
  Login,
  RegisterBaby,
  RegisterSuccess,
  RegisterUser,
  Menu,
  BabyDoctors,
  Scanner,
  Landing,
  RercordHistory,
  Feed,
  FeedDetails,
  BabyRecord,
  RequestAppointment,
} from "./Views";
import {
  AppointmentDetails,
  ReportDetails,
  PrescriptionDetails,
  VaccineDetails,
} from "./Components";
import { enableScreens } from "react-native-screens";
enableScreens();

const PERSISTENCE_KEY = "NAVIGATION_STATE";

export default function App() {
  //states
  //isReady checks if the app finished loading to render the corresponding items
  const [userData, storeItem, removeItem] = useStorage();
  const [logged, setLogged] = React.useState(false);

  let [fontsLoaded] = useFonts({
    "Roboto-Regular": require("./assets/fonts/Roboto-Regular.ttf"),
    "Arial-Rounded-Bold": require("./assets/fonts/Arial-Rounded-Bold.ttf"),
  });
  const Stack = createStackNavigator();
  if (!fontsLoaded) {
    return null;
  } else {
    return (
      <NativeBaseProvider theme={theme}>
        <AuthUserContext.Provider
          value={{
            userData: userData,
            updateItem: storeItem,
            removeItem: removeItem,
            setLogged: setLogged,
          }}
        >
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName={!logged ? "Landing" : "BabyMenu"}
              screenOptions={{
                headerShown: false,
              }}
            >
              {!logged ? (
                <>
                  <Stack.Screen name="Landing">
                    {(props) => <Landing {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="SignIn">
                    {(props) => <Login {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="GuestRegister">
                    {(props) => <RegisterUser {...props} />}
                  </Stack.Screen>
                </>
              ) : (
                <>
                  <Stack.Screen name="BabyMenu">
                    {(props) => <Menu {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="BabyRecord">
                    {(props) => <BabyRecord {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="RercordHistory">
                    {(props) => <RercordHistory {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="FeedDetails">
                    {(props) => <FeedDetails {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="RegisterUser">
                    {(props) => <RegisterUser {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="RegisterBaby">
                    {(props) => <RegisterBaby {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="QuickActionsView">
                    {(props) => <QuickActionsView {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="AppointmentDetails">
                    {(props) => <AppointmentDetails {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="VaccineDetails">
                    {(props) => <VaccineDetails {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="PrescriptionDetails">
                    {(props) => <PrescriptionDetails {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="ReportDetails">
                    {(props) => <ReportDetails {...props} />}
                  </Stack.Screen>

                  <Stack.Screen name="BabyDoctors">
                    {(props) => <BabyDoctors {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="Scan">
                    {(props) => <Scanner {...props} />}
                  </Stack.Screen>
                  <Stack.Screen name="RequestAppointment">
                    {(props) => <RequestAppointment {...props} />}
                  </Stack.Screen>
                </>
              )}
              <Stack.Screen name="RegisterSuccess">
                {(props) => <RegisterSuccess {...props} />}
              </Stack.Screen>
            </Stack.Navigator>
          </NavigationContainer>
        </AuthUserContext.Provider>
      </NativeBaseProvider>
    );
  }

  //
}
