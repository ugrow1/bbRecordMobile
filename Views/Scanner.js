import { BarCodeScanner } from "expo-barcode-scanner";
import { Modal, Button, Text, VStack, Pressable, Icon } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { StyleSheet } from "react-native";

import React from "react";

export default function Scanner({
  navigation,

  route: {
    params: { onScan, onGoBack },
  },
}) {
  const [scannedData, setScannedData] = React.useState();

  const handleQrCodeScanned = (data) => {
    onScan(data);
    if (onGoBack) {
      onGoBack();
    }
    navigation.goBack();
  };

  const ScanConfirmationModal = () => {
    return (
      <Modal isOpen={true} onClose={() => {}}>
        <Modal.Content>
          <Modal.Body>
            <Text fontSize="lg">Confirmar escaneo de código qr</Text>
          </Modal.Body>
          <Modal.Footer>
            <Button.Group variant="ghost" space={2}>
              <Button onPress={navigation.goBack}>Cancelar</Button>
              <Button
                onPress={() => {
                  handleQrCodeScanned(scannedData);
                }}
              >
                Confirmar
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    );
  };

  return (
    <VStack flex={1} justifyContent="center" bg="#000000">
      <Text color="#ffffff" fontSize="xl" variant="recordHeading">
        ¡Escanee el código QR!
      </Text>
      <Pressable
        flex={1}
        onPress={navigation.goBack}
        position="absolute"
        left={5}
        top={5}
        zIndex={1}
      >
        <Icon
          color="#ffffff"
          as={<MaterialIcons name="arrow-back-ios" />}
          size="28px"
        />
      </Pressable>

      {!scannedData ? (
        <BarCodeScanner
          barCodeTypes={["qr"]}
          onBarCodeScanned={({ type, data }) => {
            setScannedData(data);
          }}
          style={StyleSheet.absoluteFillObject}
        />
      ) : (
        <ScanConfirmationModal />
      )}
    </VStack>
  );
}
