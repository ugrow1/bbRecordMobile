import {
  Text,
  Box,
  Center,
  ScrollView,
  Pressable,
  HStack,
  Icon,
  VStack,
} from "native-base";
import React, { useEffect, useState, useContext } from "react";
import axios from "axios";
import { ViewsHeader } from "../Components";
import { AuthUserContext, route } from "../Hooks";
import { MaterialIcons } from "@expo/vector-icons";
import moment from "moment";
import "moment/locale/es";

export default function QuickActionsView({
  view,
  route: {
    params: { currentBaby },
  },
  navigation,
}) {
  return (
    <Box flex={1}>
      <Box flex={1}>
        <ViewsHeader
          color="#00BD21"
          viewTitle="Expediente"
          baby={{
            name: currentBaby.name,
            image: currentBaby.image,
          }}
          canGoBack
        />
      </Box>

      <Box flex={10} px={5} bg="#FBFBFB">
        <HStack flex={1} space={2} alignItems="center">
          <Pressable
            variant="recordSection"
            onPress={() => {
              navigation.navigate("RercordHistory", {
                recordSection: "measures",
                color: "#00BD21",
                data: currentBaby.measures,
                navigation: navigation,
              });
            }}
          >
            <Box variant="recordMostRecentContainer">
              <Text variant="recordHeading" fontSize="lg" bold mb={4}>
                MEDIDAS
              </Text>

              <Text variant="mostRecentContent">
                {currentBaby.measures.length > 0
                  ? `${
                      currentBaby.measures[currentBaby.measures.length - 1].size
                    } cm \n\n${moment(
                      currentBaby.measures[currentBaby.measures.length - 1]
                        .created_at
                    ).format("D MMMM YYYY")}`
                  : "Aún no tiene registros"}
              </Text>
            </Box>
          </Pressable>
          <Pressable
            variant="recordSection"
            onPress={() => {
              navigation.navigate("RercordHistory", {
                recordSection: "weight",
                color: "#00BD21",
                data: currentBaby.weights,
                navigation: navigation,
              });
            }}
          >
            <Box variant="recordMostRecentContainer">
              <Text variant="recordHeading" fontSize="lg" bold mb={4}>
                PESO
              </Text>

              <Text variant="mostRecentContent">
                {currentBaby.weights.length > 0
                  ? `${
                      currentBaby.weights[currentBaby.weights.length - 1].pounds
                    } lbs \n\n${moment(
                      currentBaby.weights[currentBaby.weights.length - 1]
                        .created_at
                    ).format("D MMMM YYYY")}`
                  : "Aún no tiene registros"}
              </Text>
            </Box>
          </Pressable>
        </HStack>
        <HStack flex={1} space={2}>
          <Pressable
            variant="recordSection"
            onPress={() => {
              navigation.navigate("RercordHistory", {
                recordSection: "cranial_pressure",
                color: "#00BD21",
                data: currentBaby.cranial_pressures,
                navigation: navigation,
              });
            }}
          >
            <Box variant="recordMostRecentContainer">
              <Text variant="recordHeading" fontSize="lg" bold mb={4}>
                PRESIÓN CRANEAL
              </Text>

              <Text variant="mostRecentContent">
                {currentBaby.cranial_pressures.length > 0
                  ? `${
                      currentBaby.cranial_pressures[
                        currentBaby.cranial_pressures.length - 1
                      ].pressure
                    } mmHg \n\n${moment(
                      currentBaby.cranial_pressures[
                        currentBaby.cranial_pressures.length - 1
                      ].created_at
                    ).format("D MMMM YYYY")}`
                  : "Aún no tiene registros"}
              </Text>
            </Box>
          </Pressable>

          <Pressable
            variant="recordSection"
            onPress={() => {
              navigation.navigate("RercordHistory", {
                recordSection: "insurances",
                color: "#00BD21",
                data: currentBaby.insurances,
                navigation: navigation,
              });
            }}
          >
            <Box variant="recordMostRecentContainer">
              <Text variant="recordHeading" fontSize="lg" bold mb={4}>
                SEGUROS
              </Text>

              <Text variant="mostRecentContent">
                {currentBaby.insurances.length > 0
                  ? `${
                      currentBaby.insurances[currentBaby.insurances.length - 1]
                        .number
                    } \n\n${moment(
                      currentBaby.insurances[currentBaby.insurances.length - 1]
                        .created_at
                    ).format("D MMMM YYYY")}`
                  : "Aún no tiene registros"}
              </Text>
            </Box>
          </Pressable>
        </HStack>
      </Box>
    </Box>
  );
}
