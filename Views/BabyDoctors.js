import React from "react";
import { ViewsHeader, SearchBar } from "../Components";
import {
  Text,
  Box,
  Pressable,
  Image,
  Button,
  HStack,
  Modal,
  Icon,
  ScrollView,
  Center,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { route, useScanDoctor } from "../Hooks";
import { LogBox, Linking } from "react-native";
import { cameraPermissions } from "../Components";
import NoRegisteredEntry from "./NoRegisteredEntry";
import axios from "axios";

LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
]);

export default function BabyDoctors({
  navigation,
  route: {
    params: { babyName, babyImage, babyId, color },
  },
}) {
  const [doctors, setDoctors] = React.useState({
    filteredDoctors: [],
    babyDoctors: "",
  });
  const [modifyDoctors, setModifyDoctors] = React.useState({
    showModal: false,
    doctorToModify: {},
    actionToDo: "",
    confirmationText: "",
    confirmationButtonText: "",
  });
  const { onScannedDoctor, addDoctorToBaby, setFetchedDoctors } =
    useScanDoctor(babyId);
  const { babyDoctors, filteredDoctors } = doctors;

  async function updateDoctors(newDoctors) {
    setDoctors({
      filteredDoctors: newDoctors,
      babyDoctors: newDoctors,
    });
    setFetchedDoctors(newDoctors);
  }

  async function getDoctors() {
    const response = await axios.get(route + "/babies/" + babyId);
    updateDoctors(response.data.doctors);
  }

  React.useEffect(() => {
    getDoctors();
  }, []);

  const scan = async () => {
    const { cameraAccess } = await cameraPermissions();
    if (cameraAccess == "granted") {
      navigation.navigate("Scan", {
        onScan: async (data) => {
          const doctorsTMP = await onScannedDoctor(data);
          updateDoctors(doctorsTMP);
        },
      });
    }
  };

  const doctorsFiltered = (filteredDoctors) => {
    setDoctors((prevState) => ({
      ...prevState,
      filteredDoctors: filteredDoctors,
    }));
  };

  const babyHasDoctor = (doctor) => {
    if (babyDoctors.find((doctorInBaby) => doctorInBaby.id == doctor.id)) {
      return true;
    }

    return false;
  };

  const removeDoctorFromBaby = async (doctorId) => {
    const index = babyDoctors.findIndex(
      (doctorInBaby) => doctorInBaby.id == doctorId
    );

    var babyDoctorsTMP = babyDoctors;

    babyDoctorsTMP.splice(index, 1);

    const response = await axios.put(route + "/babies/" + babyId, {
      doctors: babyDoctorsTMP,
    });
    return response.data.doctors;
  };

  /**Uses the data given on the read to add that doctor to the baby
   * @data = {doctor's Id}
   */
  const {
    showModal,
    doctorToModify: doctorToAdd,
    actionToDo,
    confirmationText,
    confirmationButtonText,
  } = modifyDoctors;

  const displayDoctors = filteredDoctors.map((doctor) => (
    <HStack key={doctor.id} justifyContent="center" h="88px" rounded="lg">
      <Box justifyContent="center" alignItems="flex-start" flex={1}>
        <Image
          source={
            doctor.meta_user.image
              ? { uri: route + doctor.meta_user.image.url }
              : require("../assets/defaultUserImage.png")
          }
          alt="Alternate Text"
          rounded="full"
          size="52px"
        />
      </Box>

      <HStack
        justifyContent="center"
        alignItems="center"
        flex={5}
        borderBottomWidth={1}
        py={1}
        borderColor="#f0f0f0"
      >
        <Box flex={2}>
          <Text bold fontSize="md" mb={1}>
            Dr. {doctor.first_name} {doctor.last_name}
          </Text>
          <Text>{doctor.meta_user.speciality.name}</Text>
        </Box>
        <HStack space={5} justifyContent="flex-end" pr="10px" flex={1}>
          {babyHasDoctor(doctor) ? (
            <HStack space={5}>
              <Icon
                onPress={() => Linking.openURL(`tel:${doctor.phone}`)}
                size={6}
                as={<MaterialIcons name="phone" />}
              />
              <Icon size={6} as={<MaterialIcons name="mail-outline" />} />
              <Icon
                onPress={() =>
                  setModifyDoctors({
                    showModal: true,
                    doctorToModify: doctor,
                    actionToDo: removeDoctorFromBaby,
                    confirmationText: "eliminar",
                    confirmationButtonText: "Eliminar",
                  })
                }
                size={6}
                as={<MaterialIcons name="delete-outline" />}
              />
            </HStack>
          ) : (
            <Icon
              onPress={() =>
                setModifyDoctors({
                  showModal: true,
                  doctorToModify: doctor,
                  actionToDo: addDoctorToBaby,
                  confirmationText: "agregar",
                  confirmationButtonText: "Agregar",
                })
              }
              size={6}
              as={<MaterialIcons name="add" />}
            />
          )}
        </HStack>
      </HStack>
    </HStack>
  ));
  if (babyDoctors) {
    return (
      <Box flex={1}>
        {/* HEADER */}

        <Box flex={1}>
          <ViewsHeader
            color={color}
            baby={{ name: babyName, image: babyImage }}
            viewTitle="Doctores"
            canGoBack
          />
        </Box>

        <Modal isOpen={showModal} onClose={() => {}}>
          <Modal.Content>
            <Modal.Body>
              <Text fontSize="lg">
                Desea {confirmationText} al doctor{" "}
                {`${doctorToAdd.first_name} ${doctorToAdd.last_name}`}?
              </Text>
            </Modal.Body>
            <Modal.Footer>
              <Button.Group variant="ghost" space={2}>
                <Button
                  onPress={() => {
                    setModifyDoctors({
                      showModal: false,
                      doctorToModify: {},
                      actionToDo: "",
                      confirmationText: "",
                      confirmationButtonText: "",
                    });
                  }}
                >
                  Cancelar
                </Button>
                <Button
                  onPress={async () => {
                    const newDoctors = await actionToDo(doctorToAdd.id);
                    updateDoctors(newDoctors);
                    setModifyDoctors({
                      showModal: false,
                      doctorToModify: {},
                      actionToDo: "",
                      confirmationText: "",
                      confirmationButtonText: "",
                    });
                  }}
                >
                  {confirmationButtonText}
                </Button>
              </Button.Group>
            </Modal.Footer>
          </Modal.Content>
        </Modal>

        <Box flex={10} bg="#fbfbfb">
          {/* IN CASE THE BABY DOESN'T HAVE ANY DOCTOR YET */}

          <Box pt={3} pb={5} bg="#ffffff" px={5}>
            <SearchBar
              placeHolder="Buscar doctores"
              endpointToConsult="/users?role.name=Doctor&_where[_or][0][first_name_contains]=searchText&_where[_or][1][last_name_contains]=searchText"
              returnResults={doctorsFiltered}
              defaultEntries={babyDoctors}
            />
          </Box>
          {babyDoctors.length < 1 ? (
            <Box flex={1} px={5}>
              {filteredDoctors == babyDoctors ? (
                <NoRegisteredEntry
                  displayText="¡Aún no tiene doctores registrados!"
                  displayIcon={<MaterialIcons name="qr-code-scanner" />}
                  action={scan}
                  color={color}
                />
              ) : (
                displayDoctors
              )}
            </Box>
          ) : (
            <>
              <ScrollView space={3} px={5}>
                {displayDoctors}
              </ScrollView>
              <Center mb={6}>
                <Pressable
                  onPress={scan}
                  justifyContent="center"
                  rounded={10}
                  size="50px"
                  bg="#6600BE"
                  w="92%"
                >
                  <HStack
                    flex={1}
                    space={4}
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Icon
                      position="absolute"
                      left={2}
                      color="#ffffff"
                      size="28px"
                      as={<MaterialIcons name="qr-code-2" />}
                    />
                    <Text color="#ffffff" fontSize="lg" bold>
                      Agregar nuevo
                    </Text>
                  </HStack>
                </Pressable>
              </Center>
            </>
          )}
        </Box>
      </Box>
    );
  } else {
    return null;
  }
}
