import React, { useState, useEffect, useContext } from "react";
import {
  Stack,
  Box,
  Button,
  Alert,
  Pressable,
  HStack,
  Icon,
  Center,
  FormControl,
  Text,
  Input,
  VStack,
} from "native-base";
import {
  ViewsHeader,
  ImageInput,
  DefaultInput,
  SelectInput,
  DateInput,
} from "../Components";
import { useRoute } from "@react-navigation/native";
import { MaterialIcons } from "@expo/vector-icons";
import { route, AuthUserContext, useFormControl, useSignIn } from "../Hooks";

import axios from "axios";
import { ScrollView, Image } from "react-native";

//This function get the names of the fields given based on their database name
//so we can have a controlled component with the states of the component being those names

export default function RegisterUser({
  route: {
    params: { viewTitle, modify },
  },
  navigation,
}) {
  const currentRoute = useRoute();
  const {
    userData: currentUser,
    logged,
    updateItem,
    setLogged,
  } = useContext(AuthUserContext);
  const logIn = useSignIn();

  const [state, setState] = useState({
    id_card: modify ? currentUser.user.id_card : "",
    first_name: modify ? currentUser.user.first_name : "",
    last_name: modify ? currentUser.user.last_name : "",
    ...(!modify && { password: "" }),
    ...(!modify && { passwordConfirmation: "" }),
    email: modify ? currentUser.user.email : "",
    phone: modify ? currentUser.user.phone : "",
    gender: modify ? currentUser.user.meta_user.gender : "Masculino",
    birth_date: modify
      ? currentUser.user.meta_user.birth_date
        ? new Date(currentUser.user.meta_user.birth_date)
        : new Date()
      : "",
    country: 1,
    city: modify ? currentUser.user.meta_user.city : "",
    address: modify ? currentUser.user.meta_user.address : "",
  });

  const [image, setImage] = useState(
    modify
      ? currentUser.user.meta_user.image
        ? currentUser.user.meta_user.image
        : require("../assets/defaultUserImage.png")
      : ""
  );

  // const [countries, setCountries] = useState();
  // const [cities, setCities] = useState();
  const [isReady, setIsReady] = useState(false);
  const [isEmailRegistered, setIsEmailRegistered] = useState(false);

  const {
    checkEmptyFields,
    emptyFields,
    saveSuccess,
    DisplayAlert,
    arePasswordsEqual,
    passwordsConfirmed,
  } = useFormControl();

  var CancelToken = axios.CancelToken;
  var source = CancelToken.source();

  // useEffect(() => {
  //   // async function getCountrys() {
  //   //   const response = await axios
  //   //     .get(route + "/countries", {
  //   //       cancelToken: source.token,
  //   //     })
  //   //     .catch((error) => console.log(error.response));
  //   //   var countryNames = [];
  //   //   var countryCities = {};
  //   //   response.data.map((country) => {
  //   //     countryNames.push({ id: country.id, name: country.name });
  //   //     countryCities[country.id] = country.cities.map((city) => ({
  //   //       id: city.id,
  //   //       name: city.name,
  //   //     }));
  //   //   });
  //   //   let initialCountry = countryNames[0].id;
  //   //   setState((prevStates) => ({
  //   //     ...prevStates,
  //   //     country: initialCountry,
  //   //   }));
  //   //   setCountries(countryNames);
  //   //   setCities(countryCities);
  //   //   setIsReady(true);
  //   // }
  //   // getCountrys();
  // }, []);

  const handleChange = (fieldName, value) => {
    setState((prevState) => ({ ...prevState, [fieldName]: value }));
  };

  const {
    id_card,
    first_name,
    last_name,
    password = "",
    passwordConfirmation = "",
    email,
    phone,
    gender,
    birth_date,
    country,
    city,
    address,
  } = state;

  //EXECUTES WHEN A NEW USER IS CREATED
  const signIn = async () => {
    const data = await logIn(email, password);
    updateItem("userData", data);
    setLogged(true);
  };

  //Exectues the code to register the entry
  /**
   *
   * @cleanStateCopy - Almacena una copia del estado, para que en caso de que falle, los campos mantengan sus valores
   */
  const config = {
    headers: {
      Authorization: `Bearer ${
        currentUser
          ? currentUser.jwt
          : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM4OTM3MjIwLCJleHAiOjE2NDE1MjkyMjB9.QyIiM19mEllBgTGt0kfbVSrEUmlysKnOLlYgxkQiYxw"
      }`,
    },
  };
  const onSubmit = async () => {
    if (isEmailRegistered) {
      return;
    }
    var metaUserData = new FormData();

    if (!modify) {
      if (!arePasswordsEqual(password, passwordConfirmation)) {
        return;
      }
    }

    //CHECKS IF ANY FIELDS ARE EMPTY
    checkEmptyFields({
      first_name,
      last_name,
      password,
      passwordConfirmation,
      email,
      gender,
    });

    if (Object.keys(emptyFields).length > 0) {
      return;
    }

    if (modify && image.uri) {
      metaUserData.append("files.image", {
        uri: image.uri,
        type: "image/jpeg",
        name: id_card + "_picture",
      });
    }

    //SETS THE METHOD AND ATTRIBUTES TO SEND WHETER ITS CREATING OR UPDATING THE USER AND META USER

    var method;
    var userData;

    if (modify) {
      //CONVERTS CITY AND COUNTRY TO INT
      setState((prevState) => ({
        ...prevState,
        country: parseInt(country),
        city: parseInt(city),
      }));

      method = axios.put;
      metaUserData.append(
        "data",
        JSON.stringify({
          city,
          address,
        })
      );
      userData = {
        id_card,
        first_name,
        last_name,
        email,
        phone,
      };
    } else {
      method = axios.post;
      metaUserData.append(
        "data",
        JSON.stringify({
          country,
          gender,
        })
      );
      userData = {
        first_name,
        last_name,
        password,
        email,
        phone,
      };
    }

    var meta_user;

    try {
      meta_user = await method(
        `${route}/meta-users${
          modify ? "/" + currentUser.user.meta_user.id : ""
        }`,
        metaUserData,
        {
          cancelToken: source.token,
        }
      );

      userData.meta_user = meta_user.data.id;
      userData.username = email.replace(/\@.*/g, "");

      var userResponse = await method(
        `${
          route +
          (modify ? "/users/" + currentUser.user.id : "/auth/local/register")
        }`,
        userData,
        {
          cancelToken: source.token,
          ...(modify && config),
        }
      );

      if (modify) {
        var currentUserTMP = currentUser;
        currentUserTMP.user = userResponse.data;
        updateItem("userData", currentUserTMP);
        saveSuccess();
      } else {
        signIn();
      }
    } catch (error) {
      if (!modify) {
        try {
          await axios.delete(route + "/meta-users/" + meta_user.data.id, {
            cancelToken: source.token,
          });
          // .catch((error) => console.log(error.response));
        } catch (error) {}
        // console.log(error.response);
      }
      // const errorInfo = error.response.data;
      // console.log(errorInfo);
      // if (errorInfo.data[0].messages[0].message == "Email is already taken.") {
      // }
    }
  };

  const checkIfEmailIsRegistered = async () => {
    const response = await axios.get(route + "/users?email=" + email, config);

    if (response.data.length > 0) {
      setIsEmailRegistered(true);
    } else {
      setIsEmailRegistered(false);
    }
  };

  // this constant generates the fields to be shown

  return (
    <Box flex={1} bg="#ffffff" px={6} w="100%" h="100%">
      {/* HEADER */}
      {currentRoute.name == "GuestRegister" ? (
        <HStack flex={1} top={16} justifyContent="center">
          <Pressable
            size="40px"
            display="flex"
            justifyContent="center"
            alignItems="center"
            position="absolute"
            left={0}
            onPress={navigation.goBack}
          >
            <Icon
              color="#3F4951"
              as={<MaterialIcons name="arrow-back-ios" />}
              size="28px"
            />
          </Pressable>
          <Text variant="landingHeader">Crea tu cuenta</Text>
        </HStack>
      ) : (
        <ViewsHeader
          viewTitle={viewTitle}
          canGoBack
          canLogOut={modify ? true : false}
        />
      )}
      <Box
        flex={currentRoute.name == "GuestRegister" ? 5 : 10}
        alignItems="center"
        w="100%"
      >
        {currentRoute.name == "GuestRegister" && (
          <Text variant="InfantyHeading" mb={7}>
            Infanty
          </Text>
        )}
        <Box bg="#ffffff" flex={20} w="100%">
          <ScrollView showsVerticalScrollIndicator={false}>
            {currentRoute.name != "GuestRegister" && (
              <Center w="100%" my={5}>
                <Stack>
                  <ImageInput
                    source={
                      image.url
                        ? {
                            uri: route + image.url,
                          }
                        : image
                    }
                    setState={setImage}
                  />
                </Stack>
              </Center>
            )}
            <Box my={3}>
              {/* Nombres */}
              <FormControl isInvalid={emptyFields["first_name"]}>
                <FormControl.Label>
                  <Text variant="label">Nombres</Text>
                </FormControl.Label>

                <Input
                  variant="formInput"
                  placeholder="Nombre"
                  value={first_name}
                  onChangeText={(text) => handleChange("first_name", text)}
                />
              </FormControl>

              {/* Apellido */}
              <FormControl isInvalid={emptyFields["last_name"]}>
                <FormControl.Label>
                  <Text variant="label">Apellidos</Text>
                </FormControl.Label>

                <Input
                  variant="formInput"
                  placeholder="Apellido"
                  value={last_name}
                  onChangeText={(text) => handleChange("last_name", text)}
                />
              </FormControl>

              {/* Genero */}

              <FormControl>
                <FormControl.Label>
                  <Text variant="label">Género</Text>
                </FormControl.Label>

                <SelectInput
                  isDisabled={modify ? true : false}
                  value={gender}
                  label="Género"
                  placeholder="Seleccione el género"
                  defaultValue="masculino"
                  setState={setState}
                  state="gender"
                  items={["Masculino", "Femenino"]}
                />
              </FormControl>

              {/* Correo electrónico */}
              <FormControl
                isInvalid={emptyFields["email"] || isEmailRegistered}
              >
                <FormControl.Label>
                  <Text variant="label">Correo electrónico</Text>
                </FormControl.Label>

                <Input
                  variant="formInput"
                  placeholder="Correo electrónico"
                  value={email}
                  onChangeText={(text) => handleChange("email", text)}
                  onEndEditing={checkIfEmailIsRegistered}
                />
                {isEmailRegistered && (
                  <Text color="#DE3232" fontSize="sm">
                    Este Correo ya ha sido registrado
                  </Text>
                )}
              </FormControl>

              {!modify && (
                <>
                  {/* Contraseña */}
                  <FormControl
                    isInvalid={emptyFields["password"] || !passwordsConfirmed}
                  >
                    <FormControl.Label>
                      <Text variant="label">Contraseña</Text>
                    </FormControl.Label>

                    <Input
                      variant="formInput"
                      type="password"
                      placeholder="Contraseña"
                      value={password}
                      onChangeText={(text) => handleChange("password", text)}
                    />
                  </FormControl>

                  {/* Confirmar contraseña */}
                  <FormControl
                    isInvalid={
                      emptyFields["passwordConfirmation"] || !passwordsConfirmed
                    }
                  >
                    <FormControl.Label>
                      <Text variant="label">Confirmar contraseña</Text>
                    </FormControl.Label>

                    <Input
                      variant="formInput"
                      type="password"
                      placeholder="Confirmar contraseña"
                      value={passwordConfirmation}
                      onChangeText={(text) =>
                        handleChange("passwordConfirmation", text)
                      }
                    />
                    {!passwordsConfirmed && (
                      <Text color="#DC2626" bold>
                        Las contraseñas no coinciden
                      </Text>
                    )}
                  </FormControl>
                </>
              )}

              {currentRoute.name != "GuestRegister" && (
                <>
                  {/* Número de teléfono */}
                  <FormControl isInvalid={emptyFields["phone"]}>
                    <FormControl.Label>
                      <Text variant="label">Número de celular</Text>
                    </FormControl.Label>

                    <Input
                      variant="formInput"
                      keyboardType="numeric"
                      placeholder="Número de teléfono"
                      value={phone}
                      onChangeText={(text) => handleChange("phone", text)}
                    />
                  </FormControl>

                  {/* Fecha de nacimiento */}
                  <FormControl>
                    <FormControl.Label>
                      <Text variant="label">Fecha de nacimiento</Text>
                    </FormControl.Label>

                    <DateInput
                      label="Fecha de nacimiento"
                      date={birth_date}
                      setState={setState}
                      state="birth_date"
                    />
                  </FormControl>

                  {/* Ciudad */}
                  <FormControl>
                    <FormControl.Label>
                      <Text variant="label">Ciudad</Text>
                    </FormControl.Label>

                    <SelectInput
                      value={parseInt(city)}
                      label="Ciudad"
                      placeholder="Seleccione su ciudad"
                      defaultValue="Santo Domingo"
                      setState={setState}
                      state="city"
                      items={isReady ? cities[country] : ["Santo Domingo"]}
                    />
                  </FormControl>

                  {/* Dirección */}
                  <FormControl isInvalid={emptyFields["address"]}>
                    <FormControl.Label>
                      <Text variant="label">Dirección</Text>
                    </FormControl.Label>

                    <Input
                      variant="formInput"
                      placeholder="Dirección"
                      value={address}
                      onChangeText={(text) => handleChange("address", text)}
                    />
                  </FormControl>
                </>
              )}
              {modify && (
                // CÉDULA
                <FormControl isInvalid={emptyFields["id_card"]}>
                  <FormControl.Label>
                    <Text variant="label">Cédula de identidad</Text>
                  </FormControl.Label>

                  <Input
                    variant="formInput"
                    keyboardType="numeric"
                    placeholder="Cédula"
                    value={id_card}
                    onChangeText={(text) => handleChange("id_card", text)}
                  />
                </FormControl>
              )}
            </Box>
          </ScrollView>
        </Box>
        <Button
          flex={1}
          mb={8}
          variant="actionButton"
          onPress={onSubmit}
          _text={{ fontSize: "18px" }}
          colorScheme="info"
        >
          {modify ? "Guardar cambios" : "Crear usuario"}
        </Button>
      </Box>
    </Box>
  );
}
