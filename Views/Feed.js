import React, { useEffect, useState, useContext } from "react";
import { Box, HStack, Text, Icon, ScrollView, Image } from "native-base";
import { AuthUserContext, route } from "../Hooks";
import { MaterialIcons } from "@expo/vector-icons";
import moment from "moment";
import "moment/locale/es";
import axios from "axios";
import { Pressable } from "react-native";

export default function Feed({ navigation }) {
  const [notifications, setNotifications] = useState();

  const {
    userData: { jwt, user },
  } = useContext(AuthUserContext);
  useEffect(() => {
    async function getNotifications() {
      const response = await axios.get(
        route +
          `/reports?report_type.name_ne=Alerta&_where[_or][0][report_type.name_ne]=Solicitud&baby.creator=${
            user.id
          }&baby.deleted=${false}`,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      );

      setNotifications(response.data);
    }

    getNotifications();
  }, []);

  if (notifications) {
    return (
      <Box bg="#ffffff">
        <ScrollView px={5} bg="#ffffff">
          {notifications.map((notification) => {
            const dateCreated = moment(notification.created_at);
            const dateOfEvent = moment(notification.date);

            return (
              <Pressable
                key={notification.id}
                onPress={() => {
                  navigation.navigate("FeedDetails", {
                    notification: notification,
                    color: "#ACACAC",
                  });
                }}
              >
                <Box borderBottomWidth={1} borderColor="#f0f0f0" pb={4} mb={4}>
                  <HStack w="100%" alignItems="center" flex={1}>
                    <Box flex={2}>
                      <Text flex={1} fontSize="lg" bold pl={1} mb={4}>
                        {notification.baby.name}:{" "}
                        {dateCreated.format("ddd").charAt(0).toUpperCase() +
                          dateCreated.format("ddd").slice(1)}{" "}
                        {dateCreated.format("D")} de{" "}
                        {dateCreated.format("MMMM")}
                      </Text>
                      <HStack>
                        <Icon size="sm" as={<MaterialIcons name="article" />} />
                        <Text pl={1}>{notification.title}</Text>
                      </HStack>
                      <HStack>
                        <Icon
                          size="sm"
                          as={<MaterialIcons name="schedule" />}
                        />
                        <Text pl={1}>
                          {dateOfEvent.format("ddd").charAt(0).toUpperCase() +
                            dateOfEvent.format("ddd").slice(1)}{" "}
                          {dateOfEvent.format("D")} de{" "}
                          {dateOfEvent.format("MMMM")}
                        </Text>
                      </HStack>
                    </Box>
                    <Box flex={1} BG="#000000" W="100%">
                      <Image
                        source={{
                          uri: route + notification.image?.url,
                        }}
                        alt="Alternate Text"
                        rounded="md"
                        size="full"
                      />
                    </Box>
                  </HStack>
                </Box>
              </Pressable>
            );
          })}
        </ScrollView>
      </Box>
    );
  }
  return null;
}
