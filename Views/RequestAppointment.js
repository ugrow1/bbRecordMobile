import React, { useEffect, useState, useContext } from "react";
import {
  Box,
  Text,
  FormControl,
  Input,
  Center,
  Pressable,
  HStack,
  TextArea,
} from "native-base";
import { Keyboard } from "react-native";
import { AuthUserContext, useFormControl, route } from "../Hooks";
import { ViewsHeader, SelectInput } from "../Components";
import axios from "axios";
export default function RequestAppointment({
  navigation,
  route: {
    params: { color, baby },
  },
}) {
  const [selectedDoctor, setSelectedDoctor] = useState();
  const [appointmentPurpose, setAppointmentPurpose] = useState();
  const [requestSent, setRequestSent] = useState();
  const [submitBottomPosition, setSubmitBottomPosition] = useState("0px");
  const {
    userData: { user, jwt },
  } = useContext(AuthUserContext);
  const { checkEmptyFields, emptyFields, saveSuccess } = useFormControl();

  useEffect(() => {
    const keyboardShowListener = Keyboard.addListener("keyboardDidShow", () => {
      setSubmitBottomPosition("50%");
    });
    const keyboardHideListener = Keyboard.addListener("keyboardDidHide", () => {
      setSubmitBottomPosition("0px");
    });
    return () => {
      keyboardShowListener.remove();
      keyboardHideListener.remove();
    };
  }, []);

  const sendRequest = async () => {
    checkEmptyFields({
      appointmentPurpose,
      selectedDoctor,
    });

    if (Object.keys(emptyFields).length > 0) {
      return;
    }

    const body = {
      title: "Solicitud de cita",
      message: appointmentPurpose,
      creator: user.id,
      doctor: selectedDoctor,
      baby: baby.id,
      report_type: 4,
    };

    const config = {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    };
    try {
      const response = await axios.post(route + "/reports", body, config);
      setRequestSent("success");
      setTimeout(navigation.goBack, 1000);
    } catch (error) {
      console.log(error.response.data);
      setRequestSent("error");
    }
  };

  const onChangePurpose = (text) => {
    setAppointmentPurpose(text);
  };
  return (
    <Box flex={1}>
      <ViewsHeader
        color={color}
        viewTitle="Solicitar cita"
        baby={{
          name: baby.name,
          image: baby.image,
        }}
        canGoBack
      />
      <Box flex={10} px={5} pt={5}>
        <FormControl>
          <Input
            editable={false}
            bg="#ffffff"
            variant="formInput"
            placeholder="Nombre"
            value={baby.name}
          />
        </FormControl>
        <SelectInput
          isInvalid={emptyFields["selectedDoctor"]}
          placeholder="Seleccione el doctor"
          setState={setSelectedDoctor}
          items={baby.doctors}
        />

        <TextArea
          isInvalid={emptyFields["appointmentPurpose"]}
          onPressOut={() => console.log("hola")}
          onPressIn={() => console.log("hola")}
          bg="#ffffff"
          placeholder="Propósito de la cita"
          value={appointmentPurpose}
          onChangeText={onChangePurpose}
        />
        {requestSent == "success" && (
          <Text color="#1ED760">¡Su solicitud se ha enviado!</Text>
        )}
        {requestSent == "error" && (
          <Text color="#DC2626">¡Ha ocurrido un error con su solicitud!</Text>
        )}
        <Center
          mb={6}
          position="absolute"
          bottom={submitBottomPosition}
          right={0}
          left={0}
        >
          <Pressable
            onPress={sendRequest}
            justifyContent="center"
            rounded={10}
            size="50px"
            bg={color}
            w="92%"
          >
            <HStack
              flex={1}
              space={4}
              justifyContent="center"
              alignItems="center"
            >
              <Text color="#ffffff" fontSize="lg" bold>
                Solicitar
              </Text>
            </HStack>
          </Pressable>
        </Center>
      </Box>
    </Box>
  );
}
