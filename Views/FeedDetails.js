import { RecordDetails } from "../Components";
import { VStack, Text, Box } from "native-base";
import { route } from "../Hooks";
import React from "react";
import moment from "moment";
import "moment/locale/es";

export default function FeedDetails({
  route: {
    params: { notification, color },
  },
  navigation,
}) {
  const notificationDate = moment(notification.created_at);
  const notificationDateOfEvent = moment(notification.date);

  return (
    <RecordDetails
      titleColor={color}
      title="Detalles de la notificación"
      image={notification.image ? route + notification.image.url : ""}
      navigation={navigation}
    >
      <VStack flex={1}>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">TÍTULO</Text>
            <Text>{notification.title}</Text>
          </VStack>
        </Box>
        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">DESCRIPCIÓN</Text>
            <Text>{notification.message}</Text>
          </VStack>
        </Box>

        <Box variant="detailItem">
          <VStack variant="detailItemContent">
            <Text variant="detailItemHeading">FECHA EMITIDA</Text>
            <Text>{`${notificationDate.format(
              "D"
            )} de ${notificationDate.format(
              "MMMM"
            )} del ${notificationDate.format("YYYY")}`}</Text>
          </VStack>
        </Box>
      </VStack>
    </RecordDetails>
  );
}
