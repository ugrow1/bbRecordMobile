import {
  VStack,
  Text,
  Box,
  HStack,
  Icon,
  Center,
  Pressable,
  ScrollView,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import React from "react";
import { RecordDetails } from "../Components";
import { route, useScanDoctor } from "../Hooks";
import moment from "moment";
import "moment/locale/es";

export default function RercordHistory({
  route: {
    params: { recordSection, color, data },
  },
  navigation,
}) {
  var currentViewTitle;
  var measureType;

  /**
   * THIS SWITCH SPECIFIES TEXT THAT WILL BE RENDERED BASED ON THE VIEW
   * @currentViewTitle SETS THE TEXT THAT WILL BE SHOWN ON THE HEADER
   * @measureType SETS THE TEXT THAT WILL HAVE THE VALUE OF THE ENTRY
   */
  switch (recordSection) {
    case "measures":
      currentViewTitle = "medidas";
      measureType = "TAMAÑO";
      break;

    case "weight":
      currentViewTitle = "peso";
      measureType = "PESO";

      break;

    case "cranial_pressure":
      currentViewTitle = "presión craneal";
      measureType = "PRESIÓN";

      break;

    case "insurances":
      currentViewTitle = "seguros";
      measureType = "NÚMERO DE SEGURO";

      break;
  }

  return (
    <RecordDetails
      titleColor={color}
      title={`Historial de ${currentViewTitle}`}
      navigation={navigation}
    >
      <VStack flex={1}>
        <ScrollView>
          {data.map((recordEntry, index) => {
            const recordEntryDate = moment(recordEntry.created_at);
            var recordEntryData;

            //THIS SWITCH TELLS WICH PART OF THE OBJECT WE SHOULD ACCESS BASED ON THE VIEW
            switch (recordSection) {
              case "measures":
                recordEntryData = `${recordEntry.size} cm`;
                break;

              case "weight":
                recordEntryData = `${recordEntry.pounds} lbs`;
                break;

              case "cranial_pressure":
                recordEntryData = `${recordEntry.pressure} mmHg`;
                break;

              case "insurances":
                recordEntryData = recordEntry.number;
                break;
            }
            return (
              <Box key={index}>
                <Box variant="detailItem">
                  <HStack justifyContent="space-between">
                    <VStack variant="detailItemContent">
                      <Text variant="detailItemHeading">FECHA DE REGISTRO</Text>
                      <Text>{`${recordEntryDate.format(
                        "D"
                      )} de ${recordEntryDate.format(
                        "MMMM"
                      )} del ${recordEntryDate.format("YYYY")}`}</Text>
                    </VStack>
                    <VStack variant="detailItemContent" mr={6}>
                      <Text variant="detailItemHeading">{measureType}</Text>
                      <Text>{recordEntryData}</Text>
                    </VStack>
                  </HStack>
                </Box>
              </Box>
            );
          })}
        </ScrollView>
      </VStack>
    </RecordDetails>
  );
}
