import { ViewsHeader, CheckBoxInput, DateInput } from "../Components";
import { StyleSheet, Modal, Platform } from "react-native";
import {
  Box,
  ScrollView,
  Center,
  Button,
  VStack,
  Input,
  Image,
  Text,
  Flex,
  TextArea,
  HStack,
  Checkbox,
} from "native-base";
import React from "react";
import axios from "axios";
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";

import { useActionsData, route, useFormControl } from "../Hooks";
export default function EntryDetails({ navigation, route: { params } }) {
  const { saveSuccess, saveError, DisplayAlert } = useFormControl();
  const setCurrentEntry = useActionsData(params.view);
  const DatabaseItem = setCurrentEntry(params.entry);
  const [injectionInfo, setInjectionInfo] = React.useState();
  const [raiseAlert, setRaiseAlert] = React.useState();
  const [doses, setDoses] = React.useState();
  const [alreadyInjected, setAlreadyInjected] = React.useState();
  const [changeDate, setChangeDate] = React.useState(false);
  React.useEffect(() => {
    async function getInjectionInfo() {
      const response = await axios.get(
        route +
          `/injected-vaccines?baby.id=${params.babyId}&vaccine.id=${params.entry.id}`,
        { headers: { Authorization: `Bearer ${params.jwt}` } }
      );
      setInjectionInfo(response.data[0]);

      var stateDoses = [];
      var injected = [];

      for (let index = 0; index < params.entry.doses.required; index++) {
        if (
          response.data.length > 0 &&
          response.data[0].injected_doses[index]
        ) {
          injected.push(index);
        }

        stateDoses.push({
          dose:
            response.data.length > 0 && response.data[0].injected_doses[index]
              ? response.data[0].injected_doses[index].dose
              : index + 1,
          date:
            response.data.length > 0 && response.data[0].injected_doses[index]
              ? response.data[0].injected_doses[index].date
              : undefined,
        });
      }
      setAlreadyInjected(injected);
      setDoses(stateDoses);
    }

    if (params.view == "vaccines") {
      getInjectionInfo();
    }
  }, []);

  const removeInjected = (values) => {
    const removedValue = values.length;
    var dosesTMP = doses;
    dosesTMP[removedValue].date = undefined;

    setDoses(dosesTMP);

    var injectedTMP = alreadyInjected;
    injectedTMP.pop();

    setAlreadyInjected(injectedTMP);

    return;
  };
  const onSubmit = async () => {
    if (injectionInfo) {
      const data = { injected_doses: doses };
      try {
        await axios.put(
          route + `/injected-vaccines/${injectionInfo.id}`,
          data,
          {
            headers: { Authorization: `Bearer ${params.jwt}` },
          }
        );
        saveSuccess();
        setRaiseAlert(true);
      } catch (error) {
        saveError();
        setRaiseAlert(true);

        console.log(error);
      }
    } else if (doses[0].date !== undefined) {
      const data = {
        baby: params.babyId,
        vaccine: params.entry.id,
        injected_doses: doses,
      };

      try {
        const response = await axios.post(route + `/injected-vaccines`, data, {
          headers: { Authorization: `Bearer ${params.jwt}` },
        });
        setInjectionInfo(response.data);
        saveSuccess();
        setRaiseAlert(true);
      } catch (error) {
        saveError();
        setRaiseAlert(true);
        console.log(error);
      }
    }
  };

  const handleCheckBoxChange = (values) => {
    if (alreadyInjected.length < values.length) {
      const addedValue = values[values.length - 1];
      setChangeDate(addedValue);
      return;
    }
    removeInjected(values);
    return;
  };

  var checkBoxes = [];

  if (params.view == "vaccines" && doses !== undefined) {
    for (let index = 0; index < params.entry.doses.required; index++) {
      checkBoxes.push(
        <VStack mt={4} key={index}>
          <HStack>
            <Flex justify="center">
              <Text fontSize="md">{index + 1} - </Text>
            </Flex>
            {/* the checkbox is enabled by the following conditions:
            1- the amount of doses of the vaccine is 1
            2- in case the checkbox isn't the first, the previous checkbox exists and has a date 
            3- in case the checkbox isn't the last, the following checkbox exists and doesn't have a date  */}
            <Checkbox
              value={index}
              isDisabled={
                doses.length == 1 ||
                (((doses[index - 1] && doses[index - 1].date) || index == 0) &&
                  doses[index + 1] &&
                  !doses[index + 1].date) ||
                index == doses.length - 1
                  ? undefined
                  : true
              }
              accessibilityLabel="This is a dummy checkbox"
              size="50px"
              colorScheme="purple"
            />
            <Input
              w="80%"
              isDisabled
              value={
                doses[index].date
                  ? new moment(doses[index].date).format("DD/MM/yyy")
                  : ""
              }
            />
          </HStack>
        </VStack>
      );
    }
  }
  const DatePicker = () => {
    return (
      <DateTimePicker
        is24Hour={true}
        display={Platform.OS === "ios" ? "inline" : "default"}
        value={new Date()}
        onChange={(event, date) => {
          var dosesTMP = doses;
          dosesTMP[changeDate].date = date;
          var injectedTMP = alreadyInjected;
          injectedTMP.push(changeDate);

          setAlreadyInjected(injectedTMP);
          setChangeDate(false);

          if (event.type != "dismissed") {
            setDoses(dosesTMP);
          }

          return;
        }}
      />
    );
  };

  return (
    <Box flex={1}>
      <ViewsHeader
        viewTitle="Detalles"
        canGoBack
        canScanDoctor={
          params.view == "appointments" ? { babyId: params.babyId } : undefined
        }
      />
      <ScrollView>
        {DatabaseItem.image && (
          <Box borderBottomWidth={1} borderColor="#6200EE">
            <Image
              source={{ uri: route + DatabaseItem.image }}
              alt="Alternate Text"
              size="full"
              h={250}
            />
          </Box>
        )}

        <Box safeArea={5}>
          <Center w="100%">
            <VStack w="100%" space={10}>
              {DatabaseItem.firstField && (
                <VStack>
                  <Text>{DatabaseItem.firstField.field}</Text>
                  <TextArea
                    aria-label="t1Disabled"
                    placeholder="Disabled TextArea"
                    value={DatabaseItem.firstField.value}
                    bg="gray.300"
                    editable={false}
                    rounded="xl"
                  />
                </VStack>
              )}
              {DatabaseItem.secondField && (
                <VStack>
                  <Text>{DatabaseItem.secondField.field}</Text>
                  <TextArea
                    aria-label="t1Disabled"
                    placeholder="Disabled TextArea"
                    value={DatabaseItem.secondField.value}
                    bg="gray.300"
                    editable={false}
                    rounded="xl"
                  />
                </VStack>
              )}

              {DatabaseItem.thirdField && (
                <VStack>
                  <Text>{DatabaseItem.thirdField.field}</Text>
                  <TextArea
                    aria-label="t1Disabled"
                    placeholder="Disabled TextArea"
                    value={DatabaseItem.thirdField.value}
                    bg="gray.300"
                    editable={false}
                    rounded="xl"
                  />
                </VStack>
              )}
              {params.view == "vaccines" ? (
                <>
                  {Platform.OS == "ios" ? (
                    <Modal
                      animationType="fade"
                      transparent
                      visible={changeDate !== false}
                      presentationStyle="overFullScreen"
                    >
                      <Flex bg="rgba(0,0,0,.2)" flex={1} justify="center">
                        <View bg="white">
                          <DatePicker />
                        </View>
                      </Flex>
                    </Modal>
                  ) : (
                    changeDate !== false && <DatePicker />
                  )}
                  <Text>Inyectadas: </Text>

                  {alreadyInjected && (
                    <Checkbox.Group
                      defaultValue={alreadyInjected}
                      accessibilityLabel="pick an item"
                      onChange={handleCheckBoxChange}
                    >
                      {checkBoxes}
                    </Checkbox.Group>
                  )}
                </>
              ) : (
                DatabaseItem.details.map((detail) => (
                  <VStack key={detail.field}>
                    <Text>{detail.field}</Text>
                    {detail.image ? (
                      <Center>
                        <Image
                          source={{ uri: route + detail.image }}
                          alt="Alternate Text"
                          size="2xl"
                        />
                      </Center>
                    ) : (
                      <TextArea
                        aria-label="t1Disabled"
                        placeholder="Disabled TextArea"
                        value={detail.value}
                        bg="gray.300"
                        editable={false}
                        rounded="xl"
                      />
                    )}
                  </VStack>
                ))
              )}
            </VStack>
          </Center>
        </Box>
      </ScrollView>
      {params.view == "vaccines" && (
        <Button onPress={onSubmit} colorScheme="info">
          Guardar cambios
        </Button>
      )}
      {raiseAlert && <DisplayAlert />}
    </Box>
  );
}

const styles = StyleSheet.create({
  appLogo: {
    alignSelf: "center",
    margin: 50,
  },
  loginForm: { padding: 15 },
  personPictureRow: {
    borderWidth: 2,
    borderRadius: 10,
    padding: 8,
    borderColor: "#666666",
    alignSelf: "center",
  },
  imageIcon: { color: "#666666" },
  registerFormItems: {
    marginLeft: 8,
    marginRight: 8,

    marginBottom: 25,
  },
  genderPicker: {
    marginLeft: 5,
  },
  registerButton: {
    marginTop: 13,
    alignSelf: "center",
  },
  floatingButtonContainer: {
    backgroundColor: "transparent",
    paddingBottom: 30,
  },
});
