import {
  Box,
  Center,
  Container,
  Icon,
  Button,
  Text,
  Heading,
  Pressable,
  ArrowForwardIcon,
  Flex,
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { signIn } from "./Login";

import React from "react";

export default function RegisterSuccess({ route }) {
  const navigation = useNavigation();


  const fields = [];

  for (let [fieldKey, field] of Object.entries(route.params.registerInfo)) {
    if (field.type !== "image") {
      fields.push(
        <Flex key={fieldKey} direction="row">
          <Text mt={3} bold>
            {fieldKey}:
          </Text>
          <Text fontSize="lg" left={2} mt={3}>
            {field}
          </Text>
        </Flex>
      );
    }
  }

  return (
    <Center flex={1} bg="#6200ee">
      <Container h="90%" w="100%">
        <Flex align="center" w="100%" flex={1} justify="flex-end">
          <Icon
            as={<Ionicons name="checkmark-circle-outline" />}
            size="150px"
            color="#00EE3C"
          />
        </Flex>

        <Box top={20} bg="#ffffff" w="100%" br="100" borderRadius={15} p={3}>
          {route.params.image && (
            <Image
              source={{
                uri: route.params.image.uri,
              }}
              alt="Imagen"
              size={"sm"}
            />
          )}
          <Heading textAlign="center" color="emerald.500">
            Registro exitoso!
          </Heading>
          <Box mt={5} mb={2} bg="#E2E2E2" borderRadius={10} p={4} pt={2}>
            {fields}
          </Box>
          <Button
            onPress={() => navigation.goBack()}
            alignSelf="flex-end"
            bg="#6200ee"
          >
            Registrar otro
          </Button>
        </Box>
        <Flex align="flex-end" w="100%" flex={1} justify="center">
          {console.log("params", route.params.id)}
          <Pressable
            onPress={() =>
              navigation.navigate("BabyMenu", { updateBabies: route.params.id })
            }
          >
            <ArrowForwardIcon color="white" size="xl" />
          </Pressable>
        </Flex>
      </Container>
    </Center>
  );
}

RegisterSuccess.defaultProps = {
  route: {
    params: {
      image: undefined,
    },
  },
};
