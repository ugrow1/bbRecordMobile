import React, { useState, useContext } from "react";
import {
  Box,
  Center,
  FormControl,
  Image,
  Input,
  Icon,
  Text,
  Button,
  IconButton,
  HStack,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { ImageBackground, Pressable } from "react-native";
import { AccountCircleOutline } from "../Components";

import { useRegisterType, route, AuthUserContext, useSignIn } from "../Hooks";

function getRegisterFields() {
  return useRegisterType("user");
}

export default function Login({ navigation }) {
  const [state, setState] = useState({
    username: "",
    password: "",
    show: true,
  });
  const { userData, updateItem, setLogged } = useContext(AuthUserContext);
  const logIn = useSignIn();
  const changePasswordVisibilty = () => {
    setState((prevState) => ({ ...prevState, show: !state.show }));
  };
  const [raiseAlert, setRaiseAlert] = useState(false);

  return (
    <Box flex={1} bg="#E9EBEA">
      <Center
        flex={1}
        bg="#ffffff"
        size="100%"
        position="absolute"
        zIndex={1}
        opacity={0.07}
      >
        <Image
          size="100%"
          alt="image"
          source={require("../assets/InfantyPhoto.jpg")}
        />
      </Center>
      <Box zIndex={2} w="100%" h="100%" px={6}>
        <HStack flex={1} top={16} justifyContent="center">
          <Pressable
            size="40px"
            display="flex"
            justifyContent="center"
            alignItems="center"
            position="absolute"
            left={0}
            onPress={navigation.goBack}
          >
            <Icon
              color="#3F4951"
              as={<MaterialIcons name="arrow-back-ios" />}
              size="28px"
            />
          </Pressable>
          <Text variant="landingHeader">Iniciar sesión</Text>
        </HStack>

        <Box flex={5} alignItems="center" w="100%">
          <Text variant="InfantyHeading">Infanty</Text>

          <Box w="100%" mt={7}>
            <Input
              variant="formInput"
              InputLeftElement={
                <Box ml={4}>
                  <AccountCircleOutline color="#0383DB" />
                </Box>
              }
              onChangeText={(text) => {
                setState((prevState) => ({ ...prevState, username: text }));
              }}
              placeholder="Correo electrónico o usuario"
            />

            <Input
              variant="formInput"
              type={state.show ? "text" : "password"}
              InputLeftElement={
                <Icon
                  ml={4}
                  color="#0383DB"
                  as={<MaterialIcons name="lock-outline" />}
                />
              }
              InputRightElement={
                <Icon
                  mr={4}
                  color="gray.300"
                  as={
                    <MaterialIcons
                      name={state.show ? "visibility-off" : "visibility"}
                    />
                  }
                  onPress={changePasswordVisibilty}
                />
              }
              onChangeText={(text) => {
                setState((prevState) => ({ ...prevState, password: text }));
              }}
              placeholder="Contraseña"
            />
            {raiseAlert && (
              <Text color="#DC2626" bold>
                El usuario o contraseña son erróneos
              </Text>
            )}
          </Box>
          <Text alignSelf="flex-start" my={8}>
            ¿Olvidaste la contraseña?
          </Text>
          <Button
            variant="actionButton"
            onPress={async () => {
              try {
                const authUser = await logIn(state.username, state.password);
                if (authUser.user.role.name != "Parent") {
                  setRaiseAlert(true);
                  return;
                }
                updateItem("userData", authUser);
                setLogged(true);
              } catch (error) {
                console.log(error);
                setRaiseAlert(true);
              }
            }}
          >
            Entrar
          </Button>
        </Box>
      </Box>
    </Box>
  );
}
