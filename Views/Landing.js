import React, { useState, useContext } from "react";
import {
  Box,
  Center,
  FormControl,
  Image,
  Input,
  Icon,
  Text,
  Button,
  HStack,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { Pressable } from "react-native";

import { useRegisterType, route, AuthUserContext, useSignIn } from "../Hooks";

function getRegisterFields() {
  return useRegisterType("user");
}

export default function Landing({ navigation }) {
  const [state, setState] = useState({
    username: "",
    password: "",
    show: true,
  });
  const { userData, updateItem, setLogged } = useContext(AuthUserContext);
  const logIn = useSignIn();
  const changePasswordVisibilty = () => {
    setState((prevState) => ({ ...prevState, show: !state.show }));
  };
  const [raiseAlert, setRaiseAlert] = useState(false);

  return (
    <Box flex={1} bg="#E9EBEA" alignItems="center">
      <Center flex={1} bg="#ffffff" size="100%" position="absolute" zIndex={1}>
        <Image
          size="100%"
          alt="image"
          source={require("../assets/InfantyPhoto.jpg")}
        />
      </Center>
      <Center
        position="absolute"
        bottom="3%"
        zIndex={2}
        rounded={18}
        w="95%"
        h="32%"
        bg="#EDE9E6"
        px={9}
      >
        <Text variant="InfantyHeading" m="8%" fontSize="6xl">
          Infanty
        </Text>
        <Button
          mb="6%"
          variant="actionButton"
          _text={{ fontSize: "22px" }}
          h="22%"
          onPress={() =>
            navigation.navigate("GuestRegister", {
              viewTitle: "Crea tu cuenta",
            })
          }
        >
          Crea tu cuenta
        </Button>
        <HStack alignItems="center" mb="6%">
          <Text fontSize="17px">¿Ya tienes una cuenta? </Text>
          <Pressable onPress={() => navigation.navigate("SignIn")}>
            <Text fontSize="17px" color="#0081DC">
              Inicia Sesión{" "}
            </Text>
          </Pressable>

          <Icon
            size="sm"
            color="#0081DC"
            as={<MaterialIcons name="trending-flat" />}
          />
        </HStack>
      </Center>
    </Box>
  );
}
