import React from "react";
import {
  Icon,
  Button,
  VStack,
  HStack,
  Pressable,
  Box,
  AddIcon,
  Text,
} from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

export default function NoRegisteredEntry({
  displayText,
  displayIcon,
  navigateTo = { route: "", props: {} },
  action,
  color = "#0484DB",
}) {
  const navigation = useNavigation();

  return (
    <Box
      flex={10}
      bg="#FBFBFB"
      px={5}
      alignItems="center"
      justifyContent="center"
    >
      <Icon size="100px" as={displayIcon} color={color} />
      <Text color="#808080" fontSize={20} mb={5}>
        {displayText}
      </Text>

      <Pressable
        onPress={() => {
          action
            ? action()
            : navigation.navigate(navigateTo.route, navigateTo.props);
        }}
        justifyContent="center"
        rounded={18}
        size="65px"
        bg={color}
        w="100%"
      >
        <HStack space={4} justifyContent="center" alignItems="center">
          <Icon
            position="absolute"
            left={5}
            color="#ffffff"
            size="34px"
            as={<MaterialIcons name="add-box" />}
          />
          <Text bold fontSize={20} color="#ffffff">
            Agregar
          </Text>
        </HStack>
      </Pressable>
    </Box>
  );
}
