import "react-native-gesture-handler";
import { StyleSheet } from "react-native";
import React, { useState } from "react";
import MenuHeader from "../Components/MenuHeader";

import { Box } from "native-base";
import { BabiesTabNavigator } from "../Components";

export default function Menu(props) {
  const [displayDoctors, setDisplayDoctors] = useState();
  const params = props.route.params;
  return (
    <Box flex={1} bg="#ffffff">
      <MenuHeader
        navigateTo={{
          route: "BabyDoctors",
          params: { babyId: displayDoctors },
        }}
        canScanDoctor={
          displayDoctors != undefined && { babyId: displayDoctors }
        }
        canAddBabies={displayDoctors != undefined}
      />
      <BabiesTabNavigator
        update={params && params.updateBabies}
        setDoctors={setDisplayDoctors}
      />
    </Box>
  );
}
const styles = StyleSheet.create({
  content: {
    backgroundColor: "red",
  },
});
