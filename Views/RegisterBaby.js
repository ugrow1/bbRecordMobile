import React, { useState, useEffect, useContext } from "react";
import {
  Stack,
  Box,
  Button,
  Text,
  Center,
  FormControl,
  ScrollView,
  Input,
  Spinner,
} from "native-base";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { socket } from "../Hooks";
import {
  ViewsHeader,
  ImageInput,
  DefaultInput,
  SelectInput,
  DateInput,
} from "../Components";
import { route, AuthUserContext, useFormControl } from "../Hooks";

import axios from "axios";

function RegisterBaby({
  route: {
    params: { modify = false, baby = undefined },
  },
}) {
  const navigation = useNavigation();
  const {
    userData: { jwt, user },
  } = useContext(AuthUserContext);
  const [state, setState] = useState({
    id_card: modify ? baby.id_card : "",
    name: modify ? baby.name : "",
    last_name: modify ? baby.last_name : "",
    gender: modify ? baby.gender : "Masculino",
    birth_date: modify ? new Date(baby.birth_date) : new Date(),
    insurance: modify
      ? baby.insurances.length > 0
        ? baby.insurances[baby.insurances.length - 1].number
        : ""
      : "",
    blood_type: modify ? baby.blood_type.name : "",
    creator: user.id,
  });
  const [image, setImage] = useState(
    modify
      ? baby.image
        ? baby.image
        : require("../assets/defaultUserImage.png")
      : ""
  );

  const [bloodTypes, setBloodTypes] = useState();
  const [raiseAlert, setRaiseAlert] = useState(false);
  const [recoverBaby, setRecoverBaby] = useState({
    searching: false,
    recoveredBabyId: "",
    invalidBaby: false,
  });

  const {
    areFieldsEmpty,
    checkEmptyFields,
    emptyFields,
    saveSuccess,
    DisplayAlert,
  } = useFormControl();

  useEffect(() => {
    async function getBloodTypes() {
      const response = await axios
        .get(route + "/blood-types", {
          cancelToken: source.token,
        })
        .catch((error) => console.log(error.response));
      var dbBloodTypes = [];

      response.data.map((bloodType) => {
        dbBloodTypes.push({ id: bloodType.id, name: bloodType.name });
      });

      let initialBloodType = dbBloodTypes[0].id;
      setState((prevStates) => ({
        ...prevStates,
        blood_type: initialBloodType,
      }));

      setBloodTypes(dbBloodTypes);
    }
    getBloodTypes();
  }, []);

  var CancelToken = axios.CancelToken;
  var source = CancelToken.source();
  const config = {
    cancelToken: source.token,
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  };

  const {
    name,
    last_name,
    gender,
    birth_date,
    insurance,
    blood_type,
    id_card,
  } = state;

  const checkIfBabyExisted = async () => {
    const response = await axios.get(
      route + "/babies?id_card=" + id_card,
      config
    );

    if (response.data.length > 0) {
      var deletedBaby = response.data[0];

      if (deletedBaby.creator.id != user.id || deletedBaby.deleted == false) {
        setRecoverBaby((prevState) => ({
          ...prevState,
          loading: false,
          invalidBaby: true,
        }));

        return;
      }

      setState({
        id_card: deletedBaby.id_card,
        name: deletedBaby.name,
        last_name: deletedBaby.last_name,
        gender: deletedBaby.gender,
        birth_date: new Date(deletedBaby.birth_date),
        insurance:
          deletedBaby.insurances.length > 0
            ? deletedBaby.insurances[deletedBaby.insurances.length - 1].number
            : "",
        blood_type: deletedBaby.blood_type,
        creator: user.id,
      });
    }

    setRecoverBaby({
      recoveredBabyId: deletedBaby?.id,
      loading: false,
    });
  };
  const onSubmit = async () => {
    //CHECKS IF ANY FIELDS ARE EMPTY
    checkEmptyFields({
      id_card,
      name,
      last_name,
      birth_date,
      blood_type,
      gender,
    });

    if (Object.keys(emptyFields).length > 0 || recoverBaby.invalidBaby) {
      return;
    }

    var data = {
      id_card,
      name,
      last_name,
      birth_date,
      blood_type,
      gender,
      creator: user.id,
    };

    if (modify) {
      //IF BABY HAS INSURANCE, WE CHECK IF THE INSURANCE IN THE INPUT ISN'T EQUAL TO THE LAST INSURANCE, SO IT DOESN'GET INSERTED AGAIN
      var currentInsuranceNumber =
        baby.insurances[baby.insurances.length - 1]?.number;

      if (insurance != "" && currentInsuranceNumber != insurance) {
        try {
          const insuranceRequest = await axios.post(
            route + "/insurances",
            {
              number: insurance,
              baby: baby.id,
            },
            config
          );
        } catch (error) {
          console.log(error);
        }
      }
    }

    var babyData = new FormData();
    babyData.append("data", JSON.stringify(data));

    if (modify && image.uri) {
      babyData.append("files.image", {
        uri: image.uri,
        type: "image/jpeg",
        name: `babyId${baby.id}`,
      });
    }

    var method;
    var endpoint;

    if (modify) {
      method = axios.put;
      endpoint = route + "/babies/" + baby.id;
    } else {
      method = axios.post;
      endpoint = route + "/babies";
    }

    try {
      var response;
      if (recoverBaby.recoveredBabyId) {
        response = await axios.put(
          route + "/babies/" + recoverBaby.recoveredBabyId,
          { deleted: false, deleted_at: null },
          config
        );
      } else {
        response = await method(endpoint, babyData, config);
      }

      saveSuccess();

      if (!modify) {
        setImage(require("../assets/defaultUserImage.png"));
        await AsyncStorage.setItem("activeBabyId", response.data.id.toString());
      }
      socket.emit("babyUpdated", socket.id);
      navigation.goBack();
    } catch (error) {
      console.log(error.response);
    }
  };
  const handleChange = (fieldName, value) => {
    setState((prevState) => ({ ...prevState, [fieldName]: value }));
  };

  if (recoverBaby.loading) {
    return (
      <Center flex={1}>
        <Spinner size="large" />
        <Text fontSize="lg">Buscando bebé</Text>
      </Center>
    );
  }
  return (
    <Box flex={1} bg="#ffffff">
      <ViewsHeader
        viewTitle={modify ? "Editar bebé" : "Registrar bebé"}
        baby={modify && baby}
        canGoBack
      />
      <Box flex={10} alignItems="center" w="100%" px={6}>
        <Box bg="#ffffff" flex={20} w="100%">
          <ScrollView showsVerticalScrollIndicator={false}>
            {modify && (
              <Center w="100%" my={5}>
                <Stack>
                  <ImageInput
                    source={
                      image.url
                        ? {
                            uri: route + image.url,
                          }
                        : image
                    }
                    setState={setImage}
                  />
                </Stack>
              </Center>
            )}
            <Box my={3}>
              {/* Nombre */}
              <FormControl isInvalid={emptyFields["name"]}>
                <FormControl.Label>
                  <Text variant="label">Nombre</Text>
                </FormControl.Label>

                <Input
                  variant="formInput"
                  placeholder="Nombre"
                  value={name}
                  onChangeText={(text) => handleChange("name", text)}
                />
              </FormControl>

              {/* Apellido */}
              <FormControl isInvalid={emptyFields["last_name"]}>
                <FormControl.Label>
                  <Text variant="label">Apellido</Text>
                </FormControl.Label>

                <Input
                  variant="formInput"
                  placeholder="Apellido"
                  value={last_name}
                  onChangeText={(text) => handleChange("last_name", text)}
                />
              </FormControl>

              {/* Genero */}
              <FormControl isInvalid={emptyFields["last_name"]}>
                <FormControl.Label>
                  <Text variant="label">Género</Text>
                </FormControl.Label>

                <SelectInput
                  value={gender}
                  label="Género"
                  placeholder="Seleccione el género"
                  defaultValue="masculino"
                  setState={setState}
                  state="gender"
                  items={["Masculino", "Femenino"]}
                />
              </FormControl>

              {/* Fecha de nacimiento */}
              <FormControl isInvalid={emptyFields["last_name"]}>
                <FormControl.Label>
                  <Text variant="label">Fecha de nacimiento</Text>
                </FormControl.Label>

                <DateInput
                  label="Fecha de nacimiento"
                  date={birth_date}
                  setState={setState}
                  state="birth_date"
                />
              </FormControl>

              {/* Tipo de sangre */}
              <FormControl isInvalid={emptyFields["last_name"]}>
                <FormControl.Label>
                  <Text variant="label">Tipo de sangre</Text>
                </FormControl.Label>

                <SelectInput
                  value={blood_type}
                  label="Tipo de sangre"
                  placeholder="Seleccione el tipo de sangre"
                  setState={setState}
                  state="blood_type"
                  items={bloodTypes ? bloodTypes : []}
                />
              </FormControl>

              {/* Cédula */}
              <FormControl>
                <FormControl.Label>
                  <Text variant="label">Cédula</Text>
                </FormControl.Label>

                <Input
                  isInvalid={recoverBaby.invalidBaby}
                  variant="formInput"
                  placeholder="Cédula"
                  value={id_card.includes(user.username) ? "" : id_card}
                  onChangeText={(text) => handleChange("id_card", text)}
                  onEndEditing={() => {
                    if (id_card.length > 0) {
                      setRecoverBaby((prevState) => ({
                        ...prevState,
                        loading: true,
                      }));
                      checkIfBabyExisted();
                    }
                  }}
                />
                {recoverBaby.recoveredBabyId && !modify ? (
                  <Text color="#1ED760" fontSize="sm">
                    Se encontró un bebé con esa cédula
                  </Text>
                ) : null}
                {recoverBaby.invalidBaby && (
                  <Text color="#DE3232" fontSize="sm">
                    Este bebé ya ha sido registrado
                  </Text>
                )}
              </FormControl>

              {/* Seguro */}
              {modify && (
                <FormControl>
                  <FormControl.Label>
                    <Text variant="label">Seguro</Text>
                  </FormControl.Label>
                  <Input
                    variant="formInput"
                    keyboardType="numeric"
                    placeholder="Seguro"
                    value={insurance}
                    onChangeText={(text) => handleChange("insurance", text)}
                  />
                </FormControl>
              )}
            </Box>
          </ScrollView>
        </Box>
        <Button
          flex={1}
          mb={8}
          variant="actionButton"
          onPress={onSubmit}
          _text={{ fontSize: "18px" }}
          colorScheme="info"
        >
          {modify ? "Guardar cambios" : "Crear bebé"}
        </Button>
      </Box>

      {raiseAlert && <DisplayAlert />}
    </Box>
  );
}

export default RegisterBaby;
