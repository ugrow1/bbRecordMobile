import {
  Text,
  Box,
  Center,
  ScrollView,
  Pressable,
  HStack,
  Icon,
} from "native-base";
import React, { useEffect, useState, useContext } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import axios from "axios";
import {
  ViewsHeader,
  SearchBar,
  Appointment,
  SufferedDiseases,
  Report,
  Injury,
  Prescription,
  Vaccine,
} from "../Components";
import { AuthUserContext, route } from "../Hooks";

export default function QuickActionsView({
  view,
  route: { params },
  navigation,
}) {
  const [ready, setReady] = useState(false);
  const {
    userData: { jwt },
  } = useContext(AuthUserContext);
  const [data, setData] = useState(true);
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();
  useEffect(() => {
    //gets the data based on the view specified view
    async function getData() {
      await axios
        .get(
          route +
            "/" +
            params.view +
            (params.view == "vaccines"
              ? ""
              : "?baby.id=" +
                params.currentBaby.id +
                (params.query ? "&" + params.query : "")),
          {
            cancelToken: source.token,
            headers: {
              Authorization: `Bearer ${jwt}`,
            },
          }
        )
        .then((response) => {
          setData(response.data);
          setReady(true);
        })
        .catch((error) => console.log(error.response));
    }
    //if the view is injury, then we dont have to fetch more data because its in the baby
    if (params.view == "injuries") {
      setData(params.currentBaby.injuries);
      setReady(true);
    } else {
      getData();
    }
  }, []);

  var Component;
  switch (params.view) {
    case "appointments":
      Component = Appointment;
      break;
    case "prescriptions":
      Component = Prescription;
      break;

    case "reports":
      Component = Report;
      break;

    case "suffered-diseases":
      Component = SufferedDiseases;
      break;

    case "vaccines":
      Component = Vaccine;
      break;

    case "injuries":
      Component = Injury;
      break;

    case "payments":
      break;
  }

  if (ready) {
    const fields =
      data.length == 0
        ? null
        : data.map((data) => {
            return (
              <Box
                key={data.id}
                borderBottomWidth={1}
                borderColor="#f0f0f0"
                pb={4}
                mb={4}
              >
                <Component
                  data={data}
                  navigation={navigation}
                  color={params.color}
                  babyId={params.currentBaby.id}
                  jwt={jwt}
                />
              </Box>
            );
          });
    return (
      <Box flex={1}>
        <ViewsHeader
          color={params.color}
          viewTitle={params.viewTitle}
          baby={{
            name: params.currentBaby.name,
            image: params.currentBaby.image,
          }}
          canGoBack
        />

        <Box flex={10} bg="#FBFBFB">
          {/* <Box pt={3}  px={5} bg="#ffffff">
            <SearchBar />
          </Box> */}

          <Box px={5} pt={5} flex={1} bg="transparent">
            <ScrollView showsVerticalScrollIndicator={false}>
              {data.length == 0 ? (
                <Center>
                  <Text>
                    No tiene
                    {params.view == "health"
                      ? "entradas"
                      : " " + params.viewTitle.toLowerCase()}
                  </Text>
                </Center>
              ) : (
                fields
              )}
            </ScrollView>
            {params.view == "appointments" && (
              <Center mb={6} position="absolute" bottom={0} right={0} left={0}>
                <Pressable
                  onPress={() => {
                    navigation.navigate("RequestAppointment", {
                      baby: params.currentBaby,
                      color: params.color,
                    });
                  }}
                  justifyContent="center"
                  rounded={10}
                  size="50px"
                  bg={params.color}
                  w="92%"
                >
                  <HStack
                    flex={1}
                    space={4}
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Icon
                      position="absolute"
                      left={2}
                      color="#ffffff"
                      size="28px"
                      as={<MaterialIcons name="event-note" />}
                    />
                    <Text color="#ffffff" fontSize="lg" bold>
                      Solicitar cita
                    </Text>
                  </HStack>
                </Pressable>
              </Center>
            )}
          </Box>
        </Box>
      </Box>
    );
  }
  return null;
}
