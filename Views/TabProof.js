import React, { useState, useEffect } from "react";
import { Header, BabySection } from "../Components";
import { Text, Box, List, HStack, Pressable, ScrollView } from "native-base";
import { MaterialIcons, AntDesign } from "@expo/vector-icons";
import { route, socket, useScanDoctor } from "../Hooks";
import { LogBox } from "react-native";
import { cameraPermissions } from "../Components";
import NoRegisteredEntry from "./NoRegisteredEntry";
import axios from "axios";

export default function TabProof() {
  const [state, setState] = useState();
  const [activeBaby, setActiveBaby] = useState(0);
  const [loading, setLoading] = useState(true);

  const getBabies = async () => {
    console.log("update");

    const response = await axios.get(route + "/babies");
    setState(response.data);
    setLoading(false);
  };

  useEffect(() => {
    getBabies();
  }, []);

  const changeActiveBaby = (babyIndex) => {
    setActiveBaby(babyIndex);
  };
  if (loading) {
    return null;
  }
  console.log(activeBaby);
  return (
    <Box flex={1}>
      <Header viewTitle={"wenas"} canGoBack canLogOut />

      <ScrollView horizontal>
        {state.map((baby, index) => (
          <Pressable
            onPress={() => changeActiveBaby(index)}
            flex={1}
            id="1"
            size="lg"
            borderColor="#000000"
            border={1}
          >
            <Text fontSize="lg">klk</Text>
          </Pressable>
        ))}
      </ScrollView>

      <BabySection baby={state[activeBaby]} />
    </Box>
  );
}
