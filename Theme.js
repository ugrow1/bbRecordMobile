import { extendTheme } from "native-base";

const headerButton = {
  h: "100%",
  alignItems: "center",
  justifyContent: "center",
};

export const theme = extendTheme({
  components: {
    Pressable: {
      variants: {
        menuHeaderButton: () => {
          return {
            ...headerButton,
            flex: 1,
          };
        },
        viewHeaderButton: () => {
          return {
            ...headerButton,
            w: "100%",
          };
        },
        recordSection: () => {
          return {
            ...headerButton,
            rounded: 20,
            borderWidth: 3,
            borderColor: "#00BD21",
            h: "80%",
            pb: 4,
            mb: 4,
            flex: 1,
          };
        },
      },
    },
    IconButton: {
      variants: {
        actionButton: () => {
          return {
            bg: "#F2F5FA",
            rounded: 18,
            size: "73px",
          };
        },
      },
    },
    Text: {
      baseStyle: {
        color: "#3F4951",
        fontFamily: "Arial-Rounded-Bold",
      },
      variants: {

        label: () => {
          return {
            fontSize: "sm",
            color: "#ACACAC"
          };
        },
        recordHeading: () => {
          return {
            position: "absolute",
            top: 5,
            right: 0,
            left: 0,
            textAlign: "center",
            color: "#989898",
          };
        },
        recordEntryItem: () => {
          return {
            fontSize: "19px",
            pl: 5,
          };
        },

        InfantyHeading: () => {
          return {
            fontSize: "4xl",
            color: "#0484DB",
          };
        },
        landingHeader: () => {
          return {
            fontSize: "xl",
          };
        },
        detailItemHeading: () => {
          return {
            fontSize: "xs",
            color: "#989898",
          };
        },
        mostRecentContent: () => {
          return {
            fontSize: "lg",
            pl: 1,
            textAlign: "center",
            color: "#656565",
          };
        },
      },
    },
    Icon: {
      variants: {
        actionIcon: () => {
          return {
            size: "36px",
          };
        },
      },
    },
    Input: {
      variants: {
        formInput: () => {
          return {
            borderWidth: 1,
            borderColor: "#EFEFEF",
            placeholderTextColor: "#3F4951",
            fontSize: 3,
            fontFamily: "Arial-Rounded-Bold",
            bg: "#ffffff",
            size: "md",
            mb: 3,
          };
        },
      },
    },
    Button: {
      variants: {
        actionButton: () => {
          return {
            _text: {
              fontFamily: "Arial-Rounded-Bold",
              color: "#FFFFFF",
              fontSize: "xl",
            },
            w: "100%",
            bg: "#0484DB",
          };
        },
      },
    },
    Box: {
      variants: {
        formInput: () => {
          return {
            borderWidth: 1,
            borderColor: "#EFEFEF",
            fontFamily: "Arial-Rounded-Bold",
            bg: "#ffffff",
            size: "50px",
            w:"100%",
            borderRadius: "5px",
            justifyContent: "center",
            pl: 4,
            mb: 3,
          };
        },
        detailItem: () => {
          return {
            borderWidth: 1,
            borderColor: "#f0f0f0",
            py: 3,
          };
        },
        recordMostRecentContainer: () => {
          return {
            w: "100%",
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            pl: 1,
          };
        },
      },
    },
    VStack: {
      variants: {
        
        detailItemContent: () => {
          return {
            px: 5,
            space: 1,
          };
        },
        recordEntryContainer: () => {
          return {
            space: 1.5,
            flex: 1,
          };
        },
      },
    },
  },
});
