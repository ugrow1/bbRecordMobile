import axios from "axios";

export default async function fetch(method, endpoint, source, body) {
  //fetch the data from database

  //gets endpoint data
  var response;
  switch (method) {
    case "post":
      if (body) {
        response = await axios.post(endpoint, body, {
          cancelToken: source.token,
        });
      } else {
        response = await axios.post(endpoint, {
          cancelToken: source.token,
        });
      }
      break;

    case "get":
      if (body) {
        response = await axios.get(endpoint, body, {
          cancelToken: source.token,
        });
      } else {
        response = await axios.get(endpoint, {
          cancelToken: source.token,
        });
      }
      break;
  }

  return response.data;
}
